import { Action, handleActions } from 'redux-actions';

import { ILocalizationReducer, ILanguage, IResources } from '@models/localization';
import * as actions from './actions';

const initialState: ILocalizationReducer = {
  languages: [],
  resources: {}
};

export const localizationReducer = handleActions<ILocalizationReducer, {}>({
  [(actions.setCurrentLanguage as any).toString()]: (state, {payload: currentLanguage}: Action<string>) => ({
    ...state,
    currentLanguage
  }),
  [(actions.setLanguages as any).toString()]: (state, {payload: languages}: Action<ILanguage[]>) => ({
    ...state,
    languages
  }),
  [(actions.setResources as any).toString()]: (state, {payload: resources}: Action<IResources>) => ({
    ...state,
    resources
  })
}, initialState);
