import { Action, handleActions } from 'redux-actions';

import { IUsersReducer, IAccount, IUserItem } from '@models/users';
import * as actions from './actions';

const initialState: IUsersReducer = {
  list: []
};

export const usersReducer = handleActions<IUsersReducer, {}>({
  [(actions.logIn as any).toString()]: (state: IUsersReducer) => ({
    ...state,
    isFetching: true
  }),
  [(actions.logInSuccess as any).toString()]: (state: IUsersReducer, {payload: account}: Action<IAccount>) => ({
    ...state,
    account,
    isFetching: false
  }),
  [(actions.logInFailure as any).toString()]: (state: IUsersReducer) => ({
    ...state,
    isFetching: false
  }),
  [(actions.logout as any).toString()]: (state: IUsersReducer) => ({
    ...state,
    account: null
  }),
  [(actions.getUsers as any).toString()]: (state: IUsersReducer) => ({
    ...state,
    isFetching: true
  }),
  [(actions.getUsersSuccess as any).toString()]: (state: IUsersReducer, {payload: list}: Action<IUserItem[]>) => ({
    ...state,
    list,
    isFetching: false
  }),
  [(actions.getUsersFailure as any).toString()]: (state: IUsersReducer) => ({
    ...state,
    isFetching: false
  }),
  [(actions.saveUser as any).toString()]: (state: IUsersReducer) => ({
    ...state,
    isFetching: true
  }),
  [(actions.saveUserSuccess as any).toString()]: (state: IUsersReducer) => ({
    ...state,
    isFetching: false
  }),
  [(actions.saveUserFailure as any).toString()]: (state: IUsersReducer) => ({
    ...state,
    isFetching: false
  })
}, initialState);
