import * as React from 'react';
import * as Moment from 'moment';
import {createStructuredSelector} from 'reselect';
const {connect} = require('react-redux');

import {IResources} from '@models/localization';
import {ICallItem, ICallsFilter} from '@models/telephony';
import {selectCurrentLanguage, selectResources} from '@data/localization/selectors';
import {selectCommentsData, selectCallsFilter} from '@data/telephony/selectors';
import {CALL_FORM_NAME} from '@data/telephony/constants';
import {Crud} from '@ui';
import {Loc} from '@containers';
import {CallForm} from './components';

interface IProps {
  currentLanguage?: string;
  resources?: IResources;
  list?: ICallItem[];
  filter?: ICallsFilter;
}

@connect(
  createStructuredSelector({
    currentLanguage: selectCurrentLanguage,
    resources: selectResources,
    list: selectCommentsData,
    filter: selectCallsFilter
  })
)
export class CallsPage extends React.Component<IProps> {

  private renderTitle = () => {
    const {filter} = this.props;
    let title = '';
    if (filter) {
      title += `${filter.dateFrom} - ${filter.dateTo} ${filter.clients.join(', ')}`;
    }
    return title;
  }

  private getString = (locKey: string) => {
    const {currentLanguage, resources} = this.props;
    return Loc.getString({currentLanguage, resources, locKey});
  }

  private durationFormatter = (value) => Moment(parseInt(value, 10) * 1000).utc().format('HH:mm:ss');

  private dateFormatter = (value) => Moment.unix(value).format('DD.MM.YYYY HH:mm:ss');

  public render() {
    const {list} = this.props;
    const colModel = [
      {
        name: 'id',
        hidden: true
      },
      {
        name: 'finish',
        hidden: true
      },
      {
        name: 'answer',
        hidden: true
      },
      {
        name: 'fromExtension',
        hidden: true
      },
      {
        name: 'disconnectReason',
        hidden: true
      },
      {
        name: 'entryId',
        hidden: true
      },
      {
        name: 'lineNumber',
        hidden: true
      },
      {
        name: 'start',
        formatter: this.dateFormatter,
        label: this.getString('dateAndTime'),
        width: 120
      },
      {
        name: 'fromNumber',
        label: this.getString('outgoing'),
        width: 120
      },
      {
        name: 'toNumber',
        label: this.getString('incoming'),
        width: 120
      },
      {
        name: 'duration',
        label: this.getString('duration'),
        formatter: this.durationFormatter,
        width: 60
      },
      {
        name: 'mark',
        label: this.getString('mark'),
        width: 100
      },
      {
        name: 'model',
        label: this.getString('model'),
        width: 100
      },
      {
        name: 'clientName',
        label: this.getString('clientName'),
        width: 100
      },
      {
        name: 'managerName',
        label: this.getString('managerName'),
        width: 100
      },
      {
        name: 'text',
        label: this.getString('comment'),
        width: 200
      },
      {
        name: 'objective',
        label: this.getString('objective'),
        width: 60
      },
      {
        name: 'moderation',
        label: this.getString('moderation'),
        width: 60
      }
    ];
    return (
      <Crud
        title={this.renderTitle()}
        colModel={colModel}
        list={list}
        formName={CALL_FORM_NAME}
        form={<CallForm/>}
      />
    );
  }
}
