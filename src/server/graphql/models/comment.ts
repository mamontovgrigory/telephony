export default (seqeulize, DataTypes) => {

  const Comment = seqeulize.define('comment', {
    start: {
      type: DataTypes.INTEGER
    },
    fromNumber: {
      type: DataTypes.STRING,
      field: 'from_number'
    },
    toNumber: {
      type: DataTypes.STRING,
      field: 'to_number'
    },
    duration: {
      type: DataTypes.INTEGER
    },
    mark: {
      type: DataTypes.STRING
    },
    model: {
      type: DataTypes.STRING
    },
    clientName: {
      type: DataTypes.STRING,
      field: 'client_name'
    },
    managerName: {
      type: DataTypes.STRING,
      field: 'manager_name'
    },
    objective: {
      type: DataTypes.STRING
    },
    moderation: {
      type: DataTypes.STRING
    },
    text: {
      type: DataTypes.STRING
    }
  });

  Comment.associate = (models) => {

    Comment.belongsTo(models.Call, {
      foreignKey: {
        name: 'entryId',
        field: 'entry_id'
      },
      targetKey: 'entryId'
    });
  };

  return Comment;
};
