import { createSelector } from 'reselect';

import { IStore } from '@models/IStore';
import { IUsersReducer, IAccount, IUserItem } from '@models/users';

const selectDomain = (state: IStore): IUsersReducer => state.users;

export const selectAccount = createSelector<IStore, IUsersReducer, IAccount>(
  selectDomain,
  (substate) => substate && substate.account
);

export const selectUsersList = createSelector<IStore, IUsersReducer, IUserItem[]>(
  selectDomain,
  (substate) => substate && substate.list
);
