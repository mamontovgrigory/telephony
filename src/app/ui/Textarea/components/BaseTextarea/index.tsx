import * as React from 'react';
import * as classNames from 'classnames';
import { uniqueId } from 'lodash';

const style = require('./style.scss');

export interface IProps {
  label?: string | React.ReactNode;
  name?: string;
  value?: string;
  disabled?: boolean;
  onChange?: (value: string) => void;
  sm?: number;
  md?: number;
  lg?: number;
}

interface IState {
}

export class BaseTextarea extends React.Component<IProps, IState> {
  private textareaId: string = uniqueId('textarea-');

  private handleChange = (e) => {
    const {onChange} = this.props;
    if (onChange) {
      onChange(e.target.value);
    }
  }

  public render() {
    const {label, value, disabled, sm, md, lg} = this.props;
    const max = 12;
    let s = sm;
    let m = md;
    let l = lg;
    if (!sm && !md && !lg) {
      s = m = l = max;
    }
    const className = classNames([
      style.Textarea,
      'input-field',
      'col',
      ['s' + s],
      ['m' + m],
      ['l' + l]
    ]);
    return (
      <div className={className}>
                <textarea id={this.textareaId}
                          className="materialize-textarea"
                          onChange={this.handleChange}
                          value={value}
                          disabled={disabled}/>
        <label htmlFor={this.textareaId}>{label}</label>
      </div>
    );
  }
}
