export interface ICommentDto {
  mark?: string;
  model?: string;
  start: number;
  fromNumber: string;
  toNumber: string;
  clientName?: string;
  managerName?: string;
  objective?: string;
  moderation?: string;
  text?: string;
  duration?: number;
  entryId?: string;
}
