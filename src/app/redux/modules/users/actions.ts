import { createAction } from 'redux-actions';

import { IAccount, IUserItem } from '@models/users';

export const logIn = createAction<{ login: string, password: string }, { resolve; reject; }>(
  'users/logIn/REQUEST',
  (data) => data,
  (_, meta) => meta
);
export const logInSuccess = createAction<IAccount>('users/logIn/SUCCESS');
export const logInFailure = createAction('users/logIn/FAILURE');

export const logout = createAction('users/logout/SUCCESS');

export const getUsers = createAction('users/getUsers/REQUEST');
export const getUsersSuccess = createAction<IUserItem[]>('users/getUsers/SUCCESS');
export const getUsersFailure = createAction('users/getUsers/FAILURE');

export const saveUser = createAction<IUserItem>('users/saveUser/REQUEST');
export const saveUserSuccess = createAction('users/saveUser/SUCCESS');
export const saveUserFailure = createAction('users/saveUser/FAILURE');

export const deleteUsers = createAction<number[]>('users/deleteUsers/REQUEST');
export const deleteUsersSuccess = createAction('users/deleteUsers/SUCCESS');
export const deleteUsersFailure = createAction('users/deleteUsers/FAILURE');
