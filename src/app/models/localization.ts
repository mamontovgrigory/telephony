export interface ILocalizationReducer {
  currentLanguage?: string;
  languages: ILanguage[];
  resources: IResources;
}

export interface ILanguage {
  code: string;
  name: string;
}

export interface IResources {
  [languageCode: string]: {
    [key: string]: any
  };
}
