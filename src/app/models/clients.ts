export interface IClientItem {
  id: number;
  phone: string;
  name?: string;
  price?: number;
  active: boolean;
  deleted: boolean;
}

export interface IClientsReducer {
  isFetching?: boolean;
  list: IClientItem[];
}
