export default(sequelize, DataTypes) => {

  const Call = sequelize.define('call', {
    records: {
      type: DataTypes.STRING
    },
    start: {
      type: DataTypes.STRING
    },
    finish: {
      type: DataTypes.STRING
    },
    answer: {
      type: DataTypes.STRING
    },
    fromExtension: {
      type: DataTypes.STRING,
      field: 'from_extension'
    },
    fromNumber: {
      type: DataTypes.STRING,
      field: 'from_number'
    },
    toExtension: {
      type: DataTypes.STRING,
      field: 'to_extension'
    },
    toNumber: {
      type: DataTypes.STRING,
      field: 'to_number'
    },
    disconnectReason: {
      type: DataTypes.STRING,
      field: 'disconnect_reason'
    },
    entryId: {
      type: DataTypes.STRING,
      field: 'entry_id',
      unique: true
    },
    lineNumber: {
      type: DataTypes.STRING,
      field: 'line_number'
    },
    location: {
      type: DataTypes.STRING
    }
  });

  return Call;
};
