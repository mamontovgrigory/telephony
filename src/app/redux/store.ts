const appConfig = require('../../../config/main');
import { createStore, applyMiddleware, compose, Middleware } from 'redux';
import { routerMiddleware } from 'react-router-redux';
import createSagaMiddleware from 'redux-saga';
const createLogger = require('redux-logger');
import thunk from 'redux-thunk';

import rootReducer from './reducers';
import { IStore } from '@models/IStore';
import { sagas } from './sagas';

export function configureStore(history, initialState?: IStore) {
  const sagaMiddleware = createSagaMiddleware();

  const middlewares: Middleware[] = [
    routerMiddleware(history),
    sagaMiddleware,
    thunk
  ];

  /** Add Only Dev. Middlewares */
  if (appConfig.env !== 'production' && process.env.BROWSER) {
    const logger = createLogger();
    middlewares.push(logger);
  }

  const composeEnhancers = (appConfig.env !== 'production' &&
    typeof window === 'object' &&
    window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__) || compose;

  const store = createStore(rootReducer, initialState, composeEnhancers(
    applyMiddleware(...middlewares)
  ));

  sagas.map(sagaMiddleware.run);

  if (appConfig.env === 'development' && (module as any).hot) {
    (module as any).hot.accept('./reducers', () => {
      store.replaceReducer((require('./reducers')));
    });
  }

  return store;
}
