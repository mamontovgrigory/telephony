import { mergeResolvers } from 'merge-graphql-schemas';

import callResolver from './call.resolver';
import clientResolver from './client.resolver';
import commentResolver from './comment.resolver';
import userResolver from './user.resolver';

export const resolvers = mergeResolvers([
  clientResolver,
  callResolver,
  commentResolver,
  userResolver
]);
