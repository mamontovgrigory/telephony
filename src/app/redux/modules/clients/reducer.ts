import { Action, handleActions } from 'redux-actions';

import { IClientsReducer, IClientItem } from '@models/clients';
import * as actions from './actions';

const initialState: IClientsReducer = {
  list: []
};

export const clientsReducer = handleActions<IClientsReducer, {}>({
  [(actions.getClients as any).toString()]: (state: IClientsReducer) => ({
    ...state,
    isFetching: true
  }),
  [(actions.getClientsSuccess as any).toString()]:
    (state: IClientsReducer, {payload: list}: Action<IClientItem[]>) => ({
    ...state,
    list,
    isFetching: false
  }),
  [(actions.getClientsFailure as any).toString()]: (state: IClientsReducer) => ({
    ...state,
    isFetching: false
  }),
  [(actions.saveClient as any).toString()]: (state: IClientsReducer) => ({
    ...state,
    isFetching: true
  }),
  [(actions.saveClientSuccess as any).toString()]: (state: IClientsReducer) => ({
    ...state,
    isFetching: false
  }),
  [(actions.saveClientFailure as any).toString()]: (state: IClientsReducer) => ({
    ...state,
    isFetching: false
  })
}, initialState);
