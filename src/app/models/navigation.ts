export interface INavigationReducer {
  navItems: INavItem[];
}

export interface INavItem {
  path?: string;
  locKey: string;
  src?: string;
  icon?: string;
  description?: string;
  to: string;
}
