import * as React from 'react';
import * as $ from 'jquery';
import * as classNames from 'classnames';
import { Link } from 'react-router-dom';
import { createStructuredSelector } from 'reselect';
const {connect} = require('react-redux');

import { ILanguage } from '@models/localization';
import { setCurrentLanguage } from '@data/localization/actions';
import { selectLanguages } from '@data/localization/selectors';
import { Container, Icon } from '@ui';
import { Loc } from '@containers';

const style = require('./style.scss');

interface IProps {
  version: string;
  logo: JSX.Element;
  account?: {
    name: string;
  };
  links?: any[];
  onExit?: () => void;
  languages?: ILanguage[];
  setCurrentLanguage?(languageCode: string): void;
}

interface IState {
}

@connect(
  createStructuredSelector({
    languages: selectLanguages
  }),
  {
    setCurrentLanguage
  }
)
export class Navbar extends React.Component<IProps, IState> {
  public componentDidMount() {
    ($('.side-nav-collapse') as any).sideNav({
      menuWidth: 300,
      edge: 'left',
      closeOnClick: false,
      draggable: true,
      onOpen: () => {
        $('#sidenav-overlay').hide();
      }
    });
    ($('.side-nav-collapse') as any).sideNav('show');
    ($('.dropdown-trigger') as  any).dropdown();
  }

  private handleClickExit = () => {
    const {onExit} = this.props;
    if (onExit) {
      onExit();
    }
  }

  public render() {
    const {version, logo, links, account, languages, setCurrentLanguage} = this.props;
    const authorized = Boolean(account);
    const panelClassName = classNames([
      'side-nav',
      'fixed',
      style.panel
    ]);
    return (
      <nav className={style.Navbar}>
        <div className="nav-wrapper">
          <Container>
            {version}
            <Link to="/" className="right" style={{height: '64px'}}>
              {logo}
            </Link>
            <ul className="right hide-on-med-and-down">
              {
                languages && languages.map((item, index) => {
                  const changeLanguage = () => setCurrentLanguage(item.code);
                  return (
                    <li key={index} onClick={changeLanguage}><a>{item.name}</a></li>
                  );
                })
              }
            </ul>
          </Container>
        </div>

        <ul className={panelClassName} hidden={!authorized}>
          {
            links && links.map((el, index) => {
              return (
                <li key={index}>
                  <Link to={el.to} className="waves-effect">
                    <i className="material-icons">{el.icon}</i>
                    <span className={style.link}>
                      <Loc locKey={el.locKey}/>
                    </span>
                  </Link>
                </li>
              );
            })
          }
          <li>
            <div className="divider"/>
          </li>
          <li>
            <a className="waves-effect" onClick={this.handleClickExit}>
              <Icon type="exit_to_app"/>
              <span className={style.link}>
                <Loc locKey="logout"/>
              </span>
            </a>
          </li>
        </ul>
      </nav>
    );
  }
}
