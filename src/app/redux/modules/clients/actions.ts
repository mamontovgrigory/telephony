import { createAction } from 'redux-actions';

import { IClientItem } from '@models/clients';

export const getClients = createAction('clients/getClients/REQUEST');
export const getClientsSuccess = createAction<IClientItem[]>('clients/getClients/SUCCESS');
export const getClientsFailure = createAction('clients/getClients/FAILURE');

export const saveClient = createAction<IClientItem>('clients/saveClient/REQUEST');
export const saveClientSuccess = createAction('clients/saveClient/SUCCESS');
export const saveClientFailure = createAction('clients/saveClient/FAILURE');

export const deleteClients = createAction<number[]>('clients/deleteClients/REQUEST');
export const deleteClientsSuccess = createAction('clients/deleteClients/SUCCESS');
export const deleteClientsFailure = createAction('clients/deleteClients/FAILURE');
