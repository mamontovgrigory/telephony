import { createSelector } from 'reselect';

import { IStore } from '@models/IStore';
import { INavigationReducer, INavItem } from '@models/navigation';

const selectDomain = (state: IStore): INavigationReducer => state.navigation;

export const selectNavItems = createSelector<IStore, INavigationReducer, INavItem[]>(
  selectDomain,
  (substate) => substate && substate.navItems
);
