import { createSelector } from 'reselect';

import { IStore } from '@models/IStore';
import { ILocalizationReducer, IResources, ILanguage } from '@models/localization';

const selectDomain = (state: IStore): ILocalizationReducer => state.localization;

export const selectCurrentLanguage = createSelector<IStore, ILocalizationReducer, string>(
  selectDomain,
  (substate) => substate && substate.currentLanguage
);

export const selectLanguages = createSelector<IStore, ILocalizationReducer, ILanguage[]>(
  selectDomain,
  (substate) => substate && substate.languages
);

export const selectResources = createSelector<IStore, ILocalizationReducer, IResources>(
  selectDomain,
  (substate) => substate && substate.resources
);
