export interface ICommentItem {
  id?: number;
  mark?: string;
  model?: string;
  clientName?: string;
  managerName?: string;
  objective?: string;
  moderation?: string;
  text?: string;
  duration?: number;
  start?: string;
  fromNumber?: string;
  toNumber?: string;
  call?: ICallItem;
}

export interface ICallItem {
  id?: number;
  records: string;
  start: string;
  finish: string;
  answer: string;
  fromExtension: string;
  fromNumber: string;
  toExtension: string;
  toNumber: string;
  disconnectReason: string;
  entryId: string;
  lineNumber: string;
  location: string;
}

export interface ICallsFilter {
  clients: string[];
  dateFrom: string;
  dateTo: string;
}

interface ICallsData {
  client?: string;
  data: Array<{
    date: string;
    calls: number;
    objective: number;
  }>;
  total: {
    calls: number,
    objective: number;
  };
  budget: number;
}

export interface ICallsTotal {
  dates: string[];
  list: ICallsData[];
  total: ICallsData;
}

export interface ITelephonyReducer {
  isFetching?: boolean;
  recordUrl?: string;
  filter?: ICallsFilter;
  commentsList: ICommentItem[];
  callsTotal?: ICallsTotal;
}
