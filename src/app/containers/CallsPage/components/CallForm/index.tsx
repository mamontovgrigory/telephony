import * as React from 'react';
import {first} from 'lodash';
const {connect} = require('react-redux');
import {createStructuredSelector} from 'reselect';

import {IResources} from '@models/localization';
import {ICommentItem} from '@models/telephony';
import {getRecord, saveComment} from '@data/telephony/actions';
import {selectCurrentLanguage, selectResources} from '@data/localization/selectors';
import {selectRecordUrl, callFormValuesSelector} from '@data/telephony/selectors';
import {CALL_FORM_NAME} from '@data/telephony/constants';
import {Form, Row, Col, AudioPlayer, Input, Select, Textarea} from '@ui';
import {Loc} from '@containers';

interface IProps {
  currentLanguage?: string;
  resources?: IResources;
  records?: string;
  recordUrl?: string;
  getRecord?(recordId: string): void;
  saveComment?(comment: ICommentItem): void;
}

@connect(
  createStructuredSelector({
    records: (state) => callFormValuesSelector(state, 'records'),
    recordUrl: selectRecordUrl,
    resources: selectResources,
    currentLanguage: selectCurrentLanguage
  }),
  {
    getRecord,
    saveComment
  }
)
export class CallForm extends React.Component<IProps> {
  public componentWillReceiveProps(nextProps: IProps) {
    const {getRecord, records} = this.props;
    const newRecords = nextProps.records;
    if (newRecords && records !== newRecords) {
      const recordId = newRecords.substring(
        newRecords.lastIndexOf('[') + 1,
        newRecords.lastIndexOf(']')
      ).split(',');
      getRecord(first(recordId));
    }
  }

  private handleSubmit = (values) => {
    const {saveComment} = this.props;
    console.log('handleSubmit', values);
    saveComment(values);
  }

  private renderAudioPlayer = () => {
    const {recordUrl, records} = this.props;
    const reg = new RegExp(records);
    const isActual = recordUrl && reg.test(recordUrl);
    return (
      <Row>
        <Col>
          {isActual && <AudioPlayer src={recordUrl}/>}
        </Col>
      </Row>
    );
  }

  private getString = (locKey: string) => {
    const {currentLanguage, resources} = this.props;
    return Loc.getString({currentLanguage, resources, locKey});
  }

  public render() {
    const objectiveOptions = [
      {
        value: '',
        label: ''
      },
      {
        value: this.getString('yes'),
        label: this.getString('yes')
      },
      {
        value: this.getString('no'),
        label: this.getString('no')
      },
      {
        value: this.getString('toModeration'),
        label: this.getString('toModeration')
      }
    ];
    const moderationOptions = [
      {
        value: '',
        label: ''
      },
      {
        value: this.getString('moderation'),
        label: this.getString('moderation')
      }
    ];
    return (
      <Form name={CALL_FORM_NAME} onSubmit={this.handleSubmit}>
        {this.renderAudioPlayer()}
        <Row>
          <Col>
            <Input sm={6} name="fromNumber" label={<Loc locKey="outgoing"/>} disabled={true}/>
            <Input sm={6} name="start" label={<Loc locKey="dateAndTime"/>} disabled={true}/>
          </Col>
        </Row>
        <Row>
          <Col>
            <Select sm={6} name="objective" label={<Loc locKey="objective"/>} options={objectiveOptions}/>
            <Select sm={6} name="moderation" label={<Loc locKey="moderation"/>} options={moderationOptions}/>
          </Col>
        </Row>
        <Row>
          <Col>
            <Input sm={6} name="mark" label={<Loc locKey="mark"/>}/>
            <Input sm={6} name="model" label={<Loc locKey="model"/>}/>
          </Col>
        </Row>
        <Row>
          <Col>
            <Input sm={6} name="clientName" label={<Loc locKey="clientName"/>}/>
            <Input sm={6} name="managerName" label={<Loc locKey="managerName"/>}/>
          </Col>
        </Row>
        <Row>
          <Col>
            <Textarea name="text" label={<Loc locKey="comment"/>}/>
          </Col>
        </Row>
      </Form>
    );
  }
}
