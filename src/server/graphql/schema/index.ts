import {makeExecutableSchema} from 'graphql-tools';
import {mergeTypes} from 'merge-graphql-schemas';

import {resolvers} from '../resolvers';
import callType from './call.type';
import clientType from './client.type';
import commentType from './comment.type';
import userType from './user.type';

export const typeDefs = mergeTypes([
  callType,
  clientType,
  commentType,
  userType
], {all: true});

export const schema = makeExecutableSchema({typeDefs, resolvers});
