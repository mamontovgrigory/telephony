import {ICallDto} from './ICallDto';

interface ICallsDataItem {
  date: string;
  calls: number;
  objective: number;
}

interface ICallsData {
  client?: string;
  data: ICallsDataItem[];
  total: {
    calls: number,
    objective: number;
  };
  budget: number;
}

export interface ICallsLogic {
  dateFormat: string;

  getClientData(dates: string[], list: ICallDto[]): ICallsDataItem[];

  getCallsTotal(dateFrom: string, dateTo: string, list: ICallDto[]): {
    list: ICallsData[];
    total: ICallsData;
  };
}
