import * as React from 'react';
import { WrappedFieldProps, BaseFieldProps } from 'redux-form';

import { Field } from '@ui';
import { BaseDatePicker, IBaseDatePickerProps } from './components';

interface IProps extends IBaseDatePickerProps {}

class ReduxFormInput extends React.Component<WrappedFieldProps & IProps> {
  public render() {
    const {
      input,
      meta: { touched, active, error },
      ...props
    } = this.props;
    const inputProps = {
      ...props,
      ...input,
      error: touched || active ? error : ''
    };
    return <BaseDatePicker {...inputProps} />;
  }
}

export const DatePicker = (props: BaseFieldProps<IProps> & IProps) => {
  return <Field component={ReduxFormInput} {...props} />;
};
