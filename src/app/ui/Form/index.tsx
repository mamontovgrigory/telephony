import * as React from 'react';
import { reduxForm } from 'redux-form';

interface IProps {
  name: string;
  initialValues?: any;
  validate?(value: any): object;
  children?: React.ReactNode;

  onSubmit(values: any): void;
}

const BaseForm = (props) => {
  const {handleSubmit, children} = props;
  return (
    <form onSubmit={handleSubmit}>
      {children}
    </form>
  );
};

export const Form = (props: IProps) => {
  const ReduxForm = reduxForm({
    form: props.name,
    validate: props.validate,
    initialValues : props.initialValues,
    enableReinitialize: true,
    destroyOnUnmount: false
  })(BaseForm);
  return (
    <ReduxForm onSubmit={props.onSubmit}>
      {props.children}
    </ReduxForm>
  );
};
