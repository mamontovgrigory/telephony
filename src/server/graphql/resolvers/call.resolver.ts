import Sequelize from 'sequelize';

import {commentsLogic, telephonyLogic} from '../../registration';

export default {
  Query: {
    getCallsTotal: async (_, {dateFrom, dateTo, duration = 0}, {models}) => {
      const list = await models.Comment.findAll({
        where: {
          start: {
            [Sequelize.Op.gte]: telephonyLogic.getTimestamp(dateFrom),
            [Sequelize.Op.lte]: telephonyLogic.getTimestamp(dateTo, true)
          },
          duration: {
            [Sequelize.Op.gte]: duration
          }
        }
      });
      return commentsLogic.getCallsTotal(dateFrom, dateTo, list);
    },
    getRecord: async (_, {recordId}) => {
      try {
        return await telephonyLogic.downloadRecord(recordId);
      } catch (error) {
        console.log('error', error);
        return false;
      }
    }
  },
  Mutation: {
    updateStatistics: async (_, {dateFrom, dateTo}, {models}) => {
      try {
        let list = await telephonyLogic.getStatistics(dateFrom, dateTo);
        const entryIdList = list.map((item) => item.entryId);
        const savedCalls = await models.Call.findAll({
          where: {
            entryId: {
              [Sequelize.Op.in]: entryIdList
            }
          }
        });
        if (savedCalls.length) {
          list = list.filter((item) => !savedCalls.includes(item.entryId));
        }
        if (list.length) {
          await models.Call.bulkCreate(list);
          const commentsList = commentsLogic.createList(list);
          await models.Comment.bulkCreate(commentsList);
        }
        return true;
      } catch (error) {
        console.log('error', error);
        return false;
      }
    }
  }
};
