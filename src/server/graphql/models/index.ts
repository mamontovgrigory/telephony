import {sequelize, Sequelize} from '../../database/Sequelize';
import initCall from './call';
import initClient from './client';
import initComment from './comment';
import initUser from './user';

const models: any = {
  Call: initCall(sequelize, Sequelize),
  Client: initClient(sequelize, Sequelize),
  Comment: initComment(sequelize, Sequelize),
  User: initUser(sequelize, Sequelize)
};

Object.keys(models).forEach((model) => {
  if ('associate' in models[model]) {
    models[model].associate(models);
  }
});

models.sequelize = sequelize;
models.Sequelize = Sequelize;

export {models};
