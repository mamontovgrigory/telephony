import * as React from 'react';
import { Field as ReduxField, BaseFieldProps } from 'redux-form';

export class Field<P = BaseFieldProps & {}> extends React.Component<BaseFieldProps & P> {
  public ComponentWithAttributes: any;

  constructor(props: BaseFieldProps & P) {
    super(props);
    const Component: any = props.component;
    this.ComponentWithAttributes = (props) => (
      <Component {...props} />
    );
  }

  public render() {
    return <ReduxField {...this.props} component={this.ComponentWithAttributes} />;
  }
}
