import * as React from 'react';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';

import { Loc } from '@containers';
import { Button } from '@ui';

interface IProps {
  header: string | React.ReactNode;
  show?: boolean;
  actions?: Array<{
    text: string | React.ReactNode;
    onClick?: () => void;
  }>;
  onClose?: () => void;
  children: any;
}

export const Modal: React.FC<IProps> = (props: IProps) => {
  const { show, header, actions, children, onClose } = props;
  return (
    <div>
      <Dialog
        open={show}
        maxWidth="md"
        fullWidth={true}
        onClose={onClose}
        aria-labelledby="form-dialog-title"
      >
        <DialogTitle id="form-dialog-title">{header}</DialogTitle>
        <DialogContent>
          {children}
        </DialogContent>
        <DialogActions>
          {actions && actions.map((action) => (<Button onClick={action.onClick}>{action.text}</Button>))}
          <Button onClick={onClose}>
            <Loc locKey="close"/>
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
};
