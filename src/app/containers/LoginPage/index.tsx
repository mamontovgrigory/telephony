import * as React from 'react';
const {connect} = require('react-redux');

import { logIn } from '@data/users/actions';
import { LOG_IN_FORM_NAME } from '@data/users/constants';
import { Row, Col, Form, Button, Input, Icon } from '@ui';
import { Loc } from '@containers';
const style = require('./style.scss');

interface IProps {
  logIn?(data: { login: string, password: string }, meta: { resolve; reject; }): void;
}

interface IState {
  wrongLoginOrPassword: boolean;
}

@connect(
  null,
  {
    logIn
  }
)
export class LoginPage extends React.Component<IProps, IState> {
  constructor(props: IProps) {
    super(props);
    this.state = {
      wrongLoginOrPassword: false
    };
  }

  private submit = (values) => {
    const {logIn} = this.props;
    new Promise((resolve, reject) => {
      logIn(values, {resolve, reject});
    }).then((result) => {
      this.setState({
        wrongLoginOrPassword: !result
      });
    });
  }

  private renderError = () => {
    return (
      <Row>
        <Col>
          <Loc locKey="wrongLoginOrPassword"/>
        </Col>
      </Row>
    );
  }

  public render() {
    const { wrongLoginOrPassword } = this.state;
    const validate = [(value) => !value];
    return (
      <div className={style.LoginPage}>
        <Row>
          <Col sm={12} md={4} offset={4}>
            <h4 className={style.title}>
              <Loc locKey="authorization"/>
            </h4>
            <Form name={LOG_IN_FORM_NAME} onSubmit={this.submit}>
              <Row>
                <Input
                  label={<Loc locKey="login"/>}
                  name="login"
                  required={true}
                  validate={validate}>
                  <Icon type="account_circle" prefix={true}/>
                </Input>
              </Row>
              <Row>
                <Input label={<Loc locKey="password"/>}
                       type="password"
                       name="password"
                       validate={validate}
                       required={true}>
                  <Icon type="vpn_key" prefix={true}/>
                </Input>
              </Row>
              {wrongLoginOrPassword && this.renderError()}
              <Row>
                <Col>
                  <Button submit={true}>
                    <Loc locKey="logIn"/>
                  </Button>
                </Col>
              </Row>
            </Form>
          </Col>
        </Row>
      </div>
    );
  }
}
