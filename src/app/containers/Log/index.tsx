/*import * as React from 'react';
import { last } from 'lodash';
const {connect} = require('react-redux');

import { ILog } from '@models/log';
import { closeLog } from '@data/log';
import { Row, Col, Modal } from '@ui';
import { Loc } from '@containers';

interface IProps {
  log?: ILog;
  closeLog?: () => void;
}

interface IState {
}

@connect(
  (state) => ({
    log: state.log
  }),
  (dispatch) => ({
    closeLog: () => dispatch(closeLog())
  })
)
export class Log extends React.Component<IProps, IState> {
  private getLastLog = () => {
    const {log: {list}} = this.props;
    return last(list);
  }

  private renderLog = () => {
    const lastLog = this.getLastLog();
    return lastLog && lastLog.log && (
      <Row>
        <Col>
          <pre>
            {lastLog.log}
          </pre>
        </Col>
      </Row>
    );
  }

  public render() {
    const {log: {show}, closeLog} = this.props;
    const lastLog = this.getLastLog();
    return (
      <Modal show={show && Boolean(lastLog)}
             header={<Loc locKey="log"/>}
             onClose={closeLog}>
        {this.renderLog()}
      </Modal>
    );
  }
}*/
