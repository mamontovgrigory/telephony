export default {
  Query: {
    getClients: async (_, __, {models}) => {
      const phones = await models.Call.findAll({
        attributes: ['toNumber'],
        group: ['toNumber']
      });
      const clients = await models.Client.findAll();
      const clientsPhone = clients.map((item) => item.toNumber);
      const newClients = phones
        .filter(({toNumber}) => !clientsPhone.includes(toNumber))
        .map(({toNumber}) => ({phone: toNumber}));
      if (newClients.length) {
        await models.Client.bulkCreate(newClients);
        return await models.Client.findAll();
      }
      return clients;
    }
  },
  Mutation: {
    saveClient: async (_, {phone, name, price, active, deleted, id}, {models}) => {
      try {
        const client = {phone, name, price, active, deleted};
        if (Number.isInteger(id)) {
          await models.Client.update(client, {where: {id}});
        } else {
          await models.Client.create(client);
        }
        return true;
      } catch (err) {
        return false;
      }
    },
    deleteClients: async (_, {ids}, {models}) => models.Client.destroy({where: {id: ids}})
  }
};
