import * as React from 'react';
import * as ReactMaterialize from 'react-materialize';
import * as classNames from 'classnames';

const style = require('./style.scss');

interface IProps {
  type: string;
  prefix?: boolean;
  right?: boolean;
}

interface IState {}

export class Icon extends React.Component<IProps, IState> {
  public render() {
    const {type, prefix, right} = this.props;
    const className = classNames({
      [style.Icon]: true,
      prefix
    });
    return (
      <ReactMaterialize.Icon className={className} right={right}>
        {type}
      </ReactMaterialize.Icon>
    );
  }
}
