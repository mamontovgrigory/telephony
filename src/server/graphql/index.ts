import {createServer} from 'http';
const {ApolloServer} = require('apollo-server-express');
import {SubscriptionServer} from 'subscriptions-transport-ws';
import {execute, subscribe} from 'graphql';
const express = require('express');

import {models} from './models';
import {schema} from './schema';
import {resolvers} from './resolvers';

const PORT = 4000;
const FORCE = false;
const endpoint = '/graphql';

const app = express();
app.use(endpoint, (req, res, next) => {
  const allowedOrigins = ['http://localhost', 'http://134.0.112.202'];
  const origin = req.headers.origin;
  if (allowedOrigins.indexOf(origin) > -1) {
    res.setHeader('Access-Control-Allow-Origin', origin);
  }
  res.header('Access-Control-Allow-Methods', 'POST, GET, OPTIONS');
  res.header('Access-Control-Allow-Credentials', 'true');
  res.header('Access-Control-Allow-Headers',
    'Content-Type, Authorization, Content-Length, X-Requested-With, X-PINGOTHER');
  if (req.method === 'OPTIONS') {
    res.sendStatus(200);
  } else {
    next();
  }
});

const ws = createServer(app);

const server = new (ApolloServer as any)({
  schema,
  resolvers,
  context: () => ({
    models
  })
});

server.applyMiddleware({app});

models.sequelize.sync({force: FORCE}).then(() => {

  ws.listen(PORT, () => {
    console.log(`Apollo Server is now running on PORT ${PORT}`);

    new SubscriptionServer({
      onConnect: () => {
        console.log('new ws connection');
        return {models};
      },
      execute,
      subscribe,
      schema
    }, {
      server: ws,
      path: endpoint
    });
  });
});
