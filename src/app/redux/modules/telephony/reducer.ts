import {Action, handleActions} from 'redux-actions';

import {ICallsTotal, ITelephonyReducer, ICommentItem, ICallsFilter} from '@models/telephony';
import * as actions from './actions';

const initialState: ITelephonyReducer = {
  commentsList: []
};

export const telephonyReducer = handleActions<ITelephonyReducer, {}>({
  [(actions.getCallsTotal as any).toString()]: (state: ITelephonyReducer) => ({
    ...state,
    isFetching: true
  }),
  [(actions.getCallsTotalSuccess as any).toString()]:
    (state: ITelephonyReducer, {payload: callsTotal}: Action<ICallsTotal>) => ({
      ...state,
      callsTotal,
      isFetching: false
    }),
  [(actions.getCallsTotalFailure as any).toString()]: (state: ITelephonyReducer) => ({
    ...state,
    isFetching: false
  }),
  [(actions.getCommentsSuccess as any).toString()]:
    (state: ITelephonyReducer, {payload: commentsList}: Action<ICommentItem[]>) => ({
      ...state,
      commentsList,
      isFetching: false
    }),
  [(actions.saveFilter as any).toString()]: (state: ITelephonyReducer, {payload: filter}: Action<ICallsFilter>) => ({
    ...state,
    filter
  }),
  [(actions.getCommentsFailure as any).toString()]: (state: ITelephonyReducer) => ({
    ...state,
    isFetching: false
  })
  ,
  [(actions.getRecord as any).toString()]: (state: ITelephonyReducer) => ({
    ...state,
    isFetching: true
  }),
  [(actions.getRecordSuccess as any).toString()]:
    (state: ITelephonyReducer, {payload: recordUrl}: Action<string>) => ({
      ...state,
      recordUrl,
      isFetching: false
    }),
  [(actions.getRecordFailure as any).toString()]: (state: ITelephonyReducer) => ({
    ...state,
    isFetching: false
  })
}, initialState);
