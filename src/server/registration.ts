import {Ioc} from './ioc';

import {ICallsLogic, ICommentsLogic, ITelephonyLogic, IWorksheetsLogic} from './interfaces';
import {CallsLogic, CommentsLogic, TelephonyLogic, WorksheetsLogic} from './modules';

export const callsLogic: ICallsLogic = Ioc.register<ICallsLogic>('ICallsLogic', true, new CallsLogic());
export const commentsLogic: ICommentsLogic = Ioc.register<ICommentsLogic>('ICommentsLogic', true, new CommentsLogic());
export const telephonyLogic: ITelephonyLogic =
  Ioc.register<ITelephonyLogic>('ITelephonyLogic', true, new TelephonyLogic());
export const worksheetsLogic: IWorksheetsLogic =
  Ioc.register<IWorksheetsLogic>('IWorksheetsLogic', true, new WorksheetsLogic());
