import {omit} from 'lodash';
import {formValueSelector} from 'redux-form';
import {createSelector} from 'reselect';

import {IStore} from '@models/IStore';
import {ITelephonyReducer, ICallsTotal, ICommentItem, ICallsFilter} from '@models/telephony';
import {CALL_FORM_NAME, SEARCH_FORM_NAME} from '@data/telephony/constants';

export const callFormValuesSelector = formValueSelector(CALL_FORM_NAME);
export const searchFormValuesSelector = formValueSelector(SEARCH_FORM_NAME);

const selectDomain = (state: IStore): ITelephonyReducer => state.telephony;

export const selectCallsTotal = createSelector<IStore, ITelephonyReducer, ICallsTotal>(
  selectDomain,
  (substate) => substate && substate.callsTotal
);

export const selectCommentsList = createSelector<IStore, ITelephonyReducer, ICommentItem[]>(
  selectDomain,
  (substate) => substate && substate.commentsList
);

export const selectCommentsData = createSelector<IStore, ITelephonyReducer, ICommentItem[], Array<object>>(
  selectCommentsList,
  (list) => list.map((item) => ({
    ...omit(item, ['call']),
    ...item.call
  }))
);

export const selectRecordUrl = createSelector<IStore, ITelephonyReducer, string>(
  selectDomain,
  (substate) => substate && substate.recordUrl
);

export const selectCallsFilter = createSelector<IStore, ITelephonyReducer, ICallsFilter>(
  selectDomain,
  (substate) => substate && substate.filter
);
