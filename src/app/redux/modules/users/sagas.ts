import {Action, ActionMeta} from 'redux-actions';
import {takeLatest, put, call} from 'redux-saga/effects';

import {Request} from '@utils/request';
import {IUserItem} from '@models/users';
import * as actions from './actions';

export function* logIn({payload: {login, password}, meta}:
                         ActionMeta<{ login: string; password: string; }, { resolve; reject; }>) {
  try {
    const response = yield Request.send({
      query:
        `{
          logIn(login: "${login}", password: "${password}") {
            id
            login
          }
        }`
    });
    const user = response.data.logIn;
    if (user) {
      const account = {
        ...user,
        permissions: []
      };
      yield put(actions.logInSuccess(account));
    } else {
      yield put(actions.logInFailure());
    }
    if (meta) {
      yield call(meta.resolve, Boolean(user));
    }
  } catch (error) {
    yield put(actions.logInFailure());
    if (meta) {
      yield call(meta.reject);
    }
  }
}

export function* getUsers() {
  try {
    const response = yield Request.send({
      query:
        `{
          getUsers {
            id
            login
          }
        }`
    });
    const list = response.data.getUsers;
    yield put(actions.getUsersSuccess(list));
  } catch (error) {
    yield put(actions.getUsersFailure());
  }
}

export function* saveUser({payload: {id, login, password}}: Action<IUserItem>) {
  try {
    yield Request.send({
      query:
        `mutation {
          saveUser(
            login: "${login}",
            password: "${password ? password : ''}"
            ${Number.isInteger(id) ? `, id: ${id}` : ''}
          )
        }`
    });
    yield put(actions.saveUserSuccess());
    yield put(actions.getUsers());
  } catch (error) {
    yield put(actions.saveUserFailure());
  }
}

export function* deleteUsers({payload: ids}: Action<number[]>) {
  try {
    yield Request.send({
      query:
        `mutation {
          deleteUsers(ids:[${ids}])
        }`
    });
    yield put(actions.deleteUsersSuccess());
    yield put(actions.getUsers());
  } catch (error) {
    yield put(actions.deleteUsersFailure());
  }
}

export default function* sagasManager() {
  yield takeLatest(actions.logIn, logIn);
  yield takeLatest(actions.getUsers, getUsers);
  yield takeLatest(actions.saveUser, saveUser);
  yield takeLatest(actions.deleteUsers, deleteUsers);
}
