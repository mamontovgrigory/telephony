import { Express } from 'express-serve-static-core';
import session = require('express-session');

export class ExpressSessionTuner {
  public static Setup(app: Express) {
    const MemoryStore = require('session-memory-store')(session);
    app.use(session({
      name: 'x-session',
      secret: 'yaouyahanSecretWord',
      resave: false,
      saveUninitialized: false,
      store: new MemoryStore({expires: 60 * 60 * 4})
    }));
  }
}
