import {Action} from 'redux-actions';
import {select, takeLatest, put} from 'redux-saga/effects';

import {Request} from '@utils/request';
import {ICallsFilter, ICommentItem} from '@models/telephony';
import {selectCallsFilter} from '@data/telephony/selectors';
import * as actions from './actions';

export function* getCallsTotal({payload: {dateFrom, dateTo}}: Action<{ dateFrom: string, dateTo: string }>) {
  try {
    const response = yield Request.send({
      query:
        `{
          getCallsTotal(dateFrom:"${dateFrom}", dateTo:"${dateTo}") {
            dates,
            list{
              client,
              data{
                date,
                calls,
                objective
              },
              total{
                calls,
                objective
              },
              budget
            },
            total{
              data{
                date,
                calls,
                objective
              },
              total{
                calls,
                objective
              },
              budget
            }
          }
        }`
    });
    const data = response.data.getCallsTotal;
    yield put(actions.getCallsTotalSuccess(data));
  } catch (error) {
    yield put(actions.getCallsTotalFailure());
  }
}

export function* getComments({payload: {clients, dateFrom, dateTo}}: Action<ICallsFilter>) {
  try {
    yield put(actions.saveFilter({dateFrom, dateTo, clients}));
    const response = yield Request.send({
      query:
        `{
          getComments(dateFrom:"${dateFrom}", dateTo:"${dateTo}", numbers:["${clients.join('","')}"]) {
            id,
            mark,
            model,
            start,
            duration, 
            fromNumber,
            toNumber, 
            clientName,
            managerName,
            objective,
            moderation,
            text,
            call{
              records,
              entryId      
            }
          }
        }`
    });
    const list = response.data.getComments;
    yield put(actions.getCommentsSuccess(list));
  } catch (error) {
    yield put(actions.getCommentsFailure());
  }
}

export function* saveComment({ payload: comment }: Action<ICommentItem>) {
  try {
    const {
      mark = '',
      model = '',
      clientName = '',
      managerName = '',
      objective = '',
      moderation = '',
      text = '',
      start = '',
      fromNumber = '',
      toNumber = '',
      id
    } = comment;
    const filter = yield select(selectCallsFilter);
    yield Request.send({
      query:
        `mutation {
          saveComment(
            mark: "${mark}",
            model: "${model}",
            clientName: "${clientName}",
            managerName: "${managerName}",
            objective: "${objective}",
            moderation: "${moderation}",
            text: "${text}"
            start: "${start}",
            fromNumber: "${fromNumber}",
            toNumber: "${toNumber}",
            ${Number.isInteger(id) ? `, id: ${id}` : ''}
          )      
        }`
    });
    yield put(actions.saveCommentSuccess());
    yield put(actions.getComments(filter));
  } catch (error) {
    yield put(actions.saveCommentFailure());
  }
}

export function* getRecord({payload: recordId}: Action<string>) {
  try {
    let recordUrl = '';
    if (recordId) {
      const response = yield Request.send({
        query:
          `{
          getRecord(recordId: "${recordId}")
        }`
      });
      recordUrl = response.data.getRecord;
    }
    yield put(actions.getRecordSuccess(recordUrl));
  } catch (error) {
    yield put(actions.getRecordFailure());
  }
}

export function* getReport({payload: {dateFrom, dateTo}}: Action<{ dateFrom: string, dateTo: string }>) {
  try {
    const response = yield Request.send({
      query:
        `{
          getReport(dateFrom:"${dateFrom}", dateTo:"${dateTo}")
        }`
    });
    const reportUrl = response.data.getReport;
    location.assign(reportUrl);
    yield put(actions.getReportSuccess());
  } catch (error) {
    yield put(actions.getReportFailure());
  }
}

export default function* sagasManager() {
  yield takeLatest(actions.getCallsTotal, getCallsTotal);
  yield takeLatest(actions.getComments, getComments);
  yield takeLatest(actions.saveComment, saveComment);
  yield takeLatest(actions.getRecord, getRecord);
  yield takeLatest(actions.getReport, getReport);
}
