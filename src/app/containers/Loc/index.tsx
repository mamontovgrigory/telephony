import * as React from 'react';
import * as i18next from 'i18next';
import { createStructuredSelector } from 'reselect';
const {connect} = require('react-redux');

import { IResources } from '@models/localization';
import { selectCurrentLanguage, selectResources } from '@data/localization/selectors';

interface IProps {
  currentLanguage?: string;
  resources?: IResources;
  locKey: string;
  options?: any;
}

interface IState {

}

@connect(
  createStructuredSelector({
    currentLanguage: selectCurrentLanguage,
    resources: selectResources
  })
)
export class Loc extends React.Component<IProps, IState> {
  public static getString({currentLanguage, resources, locKey, options}: IProps): string {
    i18next.init({
      lng: currentLanguage,
      resources
    });
    return i18next.t(locKey, options);
  };

  public render() {
    const {currentLanguage, resources, locKey, options} = this.props;
    i18next.init({
      lng: currentLanguage,
      resources
    });
    return (
      <span>{i18next.t(locKey, options)}</span>
    );
  }
}
