import * as React from 'react';
import { Switch, Route } from 'react-router-dom';

import {
  App,
  CallsPage,
  ClientsPage,
  HomePage,
  TelephonyPage,
  UsersPage
} from '@containers';

export default (
  <App>
    <Switch>
      <Route path="/telephony" component={TelephonyPage} />
      <Route path="/calls" component={CallsPage} />
      <Route path="/clients" component={ClientsPage} />
      <Route path="/users" component={UsersPage} />
      <Route path="/" component={HomePage} />
    </Switch>
  </App>
);
