import * as React from 'react';
import { Link } from 'react-router-dom';
import MaterialMenu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';

import { Loc } from '@containers';

interface IProps {
  menuItems: Array<{
    icon: string;
    locKey: string;
    to: string;
  }>;
}

export class Menu extends React.Component<IProps> {
  public state = {
    anchorEl: null
  };

  public handleClick = (event) => {
    this.setState({ anchorEl: event.currentTarget });
  }

  public handleClose = () => {
    this.setState({ anchorEl: null });
  }

  public renderMenuItem = (item, index) => (
    <MenuItem key={index} onClick={this.handleClose}>
      <Link to={item.to}>
        <Loc locKey={item.locKey}/>
      </Link>
    </MenuItem>
  )

  public render() {
    const { menuItems } = this.props;
    const { anchorEl } = this.state;

    return (
      <div>
        <IconButton
          aria-owns={anchorEl ? 'simple-menu' : undefined}
          aria-haspopup="true"
          onClick={this.handleClick}
        >
          <MenuIcon />
        </IconButton>
        <MaterialMenu
          id="simple-menu"
          anchorEl={anchorEl}
          open={Boolean(anchorEl)}
          onClose={this.handleClose}
        >
          {menuItems.map(this.renderMenuItem)}
        </MaterialMenu>
      </div>
    );
  }
}
