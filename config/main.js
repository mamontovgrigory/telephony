/** General Configurations Like PORT, HOST names and etc... */

var config = {
  env: process.env.NODE_ENV || 'development',
  host: process.env.HOST || 'localhost',
  port: process.env.PORT || 80,
  karmaPort: 9876,

  // This part goes to React-Helmet for Head of our HTML
  app: {
    head: {
      title: 'Online idea',
      titleTemplate: '%s',
      meta: [
        { charset: 'utf-8' },
        { 'http-equiv': 'x-ua-compatible', content: 'ie=edge' },
        { name: 'viewport', content: 'width=device-width, initial-scale=1' },
        { name: 'description', content: 'React Redux Typescript' },
      ]
    }
  },

  database: {
    user: 'postgres',
    database: 'telephony',
    password: 'kavelpopkov',
    host: 'localhost',
    port: 5432,
    max: 200,
    idleTimeoutMillis: 30000,
    connectionTimeoutMillis: 2000
  }
};

module.exports = config;
