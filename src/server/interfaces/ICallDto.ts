export interface ICallDto {
  id?: number;
  records: string;
  start: string;
  finish: string;
  answer: string;
  fromExtension: string;
  fromNumber: string;
  toExtension: string;
  toNumber: string;
  disconnectReason: string;
  entryId: string;
  lineNumber: string;
  location: string;
  duration?: number;
}
