import xlsx from 'node-xlsx';

import { IWorksheetsLogic } from '../interfaces/IWorksheetsLogic';

export class WorksheetsLogic implements IWorksheetsLogic {
  public parse(file: string): Array<{ name: string, data: any[] }> {
    const worksheets = xlsx.parse(file);
    try {
      if (worksheets && worksheets.length) {
        return worksheets;
      }
    } catch (e) {
      console.log(e);
    }
  }

  public getMappedBuffer(name, mapping, list) {
    const data = [Object.values(mapping)];
    list.forEach((item) => {
      const dataItem = [];
      Object.keys(mapping).forEach((key) => {
        dataItem.push(item[key]);
      });
      data.push(dataItem);
    });
    return this.getBuffer(name, data);
  }

  public getBuffer(name, data) {
    return (xlsx as any).build([{name, data}]);
  }

  public getMappedData(mapping, file) {
    const worksheets = this.parse(file);
    const worksheet = worksheets && worksheets.length ? worksheets[0] : null;
    const result = [];
    // for (const worksheet of worksheets) {
    if (worksheet) {
      const {data} = worksheet;
      if (data && data.length) {
        const headerIndex = 0;
        const headers = data[headerIndex];
        for (let index = 1; index < data.length; index++) {
          const row = data[index];
          const item = {};
          for (const key of Object.keys(mapping)) {
            const keyIndex = headers.indexOf(mapping[key]);
            const value = row[keyIndex];
            item[key] = value ? value : '';
          }
          result.push(item);
        }
      }
    }
    // }
    return result;
  }
}
