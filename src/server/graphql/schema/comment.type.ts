export default `
  type Comment {
    id: Int
    entryId: String
    start: Int!
    fromNumber: String!
    toNumber: String!
    duration: Int
    mark: String
    model: String
    clientName: String
    managerName: String
    objective: String
    moderation: String
    text: String
    call: Call
  }

  type Query {
    getComments(dateFrom: String!, dateTo: String!, numbers: [String]!): [Comment]!
    getReport(dateFrom: String!, dateTo: String!): String!
  }

  type Mutation {
    saveComment(
      mark: String!,
      model: String!,
      clientName: String!,
      managerName: String!,
      objective: String!,
      moderation: String!,
      text: String!,
      start: String!,
      fromNumber: String!,
      toNumber: String!,
      id: Int
    ): Boolean!
  }

  type Subscription {
    commentAdded: Comment!
  }
`;
