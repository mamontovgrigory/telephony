import * as React from 'react';
import * as ReactMaterialize from 'react-materialize';
import * as classNames from 'classnames';

interface IProps {
  sm?: number;
  md?: number;
  lg?: number;
  offset?: number;
  className?: string;
  align?: 'right' | 'left' | 'center';
}

interface IState {}

export class Col extends React.Component<IProps, IState> {
  public render() {
    const {sm, md, lg, offset, align, className, children} = this.props;
    const max = 12;
    let s = sm;
    let m = md;
    let l = lg;
    let offsetString = '';
    if (offset) {
      if (sm && sm < max) {
        offsetString += ' s' + offset;
      }
      if (md && md < max) {
        offsetString += ' m' + offset;
      }
      if (lg && lg < max) {
        offsetString += ' l' + offset;
      }
    }
    if (!sm && !md && !lg) {
      s = m = l = max;
    }
    const cn = classNames(align ? align + '-align' : '', className);
    return (
      <ReactMaterialize.Col s={s} m={m} l={l} offset={offsetString} className={cn}>
        {children}
      </ReactMaterialize.Col>
    );
  }
}
