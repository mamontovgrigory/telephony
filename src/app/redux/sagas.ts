import clientsSagas from './modules/clients/sagas';
import telephonySagas from './modules/telephony/sagas';
import usersSagas from './modules/users/sagas';

export const sagas = [
  clientsSagas,
  telephonySagas,
  usersSagas
];
