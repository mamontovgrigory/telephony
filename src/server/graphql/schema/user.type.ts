export default `
# User type, describes the fields for it.
  type User {
    # Each user gets an id genereated by database.
    id: Int!
    # The user's login, should be typed in the login field.
    login: String!
    # The user's password, should be typed in the password field.
    password: String!
  }
# Mutations for the User type
  type Mutation {
    # It logs a unique login and returns a boolean (true or false).
    saveUser(login:String!, password:String!, id:Int): Boolean!
    deleteUsers(ids:[Int]!): Boolean!
  }

  type Query {
    getUser(id:Int!): User
    logIn(login:String!, password:String!): User
    getUsers: [User]!
  }
`;
