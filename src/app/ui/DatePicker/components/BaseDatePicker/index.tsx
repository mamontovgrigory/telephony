import * as React from 'react';
import * as classNames from 'classnames';
import * as ReactMaterialize from 'react-materialize';

const style = require('./style.scss');

export interface IProps {
  label?: string | React.ReactNode;
  name?: string;
  value?: string;
  disabled?: boolean;
  required?: boolean;
  error?: boolean;
  onChange?: (value: string) => void;
  sm?: number;
  md?: number;
  lg?: number;
  getRef?: (input: HTMLInputElement) => void;
}

interface IState {
}

export class BaseDatePicker extends React.Component<IProps, IState> {
  private setRef = (input) => {
    const {getRef} = this.props;
    if (getRef && input && input.input) {
      getRef(input.input);
    }
  }

  private handleChange = (e) => {
    const {onChange} = this.props;
    if (onChange) {
      onChange(e.target.value);
    }
  }

  public render() {
    const {label, name, value, error, disabled, sm, md, lg} = this.props;
    const max = 12;
    let s = sm;
    let m = md;
    let l = lg;
    if (!sm && !md && !lg) {
      s = m = l = max;
    }
    const className = classNames({
      [style.DatePicker]: true,
      [style.error]: error
    });
    return (
      <ReactMaterialize.Input
        className={className}
        name={name}
        label={label}
        options={{format: 'dd.mm.yyyy'}}
        type="date"
        disabled={disabled}
        onChange={this.handleChange}
        value={value}
        ref={this.setRef}
        s={s}
        m={m}
        l={l}
      />
    );
  }
}
