import * as React from 'react';
import { WrappedFieldProps, BaseFieldProps } from 'redux-form';

import { Field } from '@ui';
import { BaseTextarea, IBaseTextareaProps } from './components';

interface IProps extends IBaseTextareaProps {}

class ReduxFormTextarea extends React.Component<WrappedFieldProps & IProps> {
  public render() {
    const {
      input,
      meta: { touched, active, error },
      ...props
    } = this.props;
    const inputProps = {
      ...props,
      ...input,
      error: touched || active ? error : ''
    };
    return <BaseTextarea {...inputProps} />;
  }
}

export const Textarea = (props: BaseFieldProps<IProps> & IProps) => {
  return <Field component={ReduxFormTextarea} {...props} />;
};
