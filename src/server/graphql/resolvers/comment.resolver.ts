import Sequelize from 'sequelize';
import {PubSub} from 'graphql-subscriptions';

import {commentsLogic, telephonyLogic} from '../../registration';

const pubsub = new PubSub();
const NEW_COMMENT = 'commentAdded';

export default {
  Comment: {
    call: async ({entryId}, _, {models}) => models.Call.findOne({where: {entryId}})
  },
  Query: {
    getComments: async (_, {dateFrom, dateTo, numbers, duration = 0}, {models}) => models.Comment.findAll({
      where: {
        start: {
          [Sequelize.Op.gte]: telephonyLogic.getTimestamp(dateFrom),
          [Sequelize.Op.lte]: telephonyLogic.getTimestamp(dateTo, true)
        },
        toNumber: {
          [Sequelize.Op.in]: numbers
        },
        duration: {
          [Sequelize.Op.gte]: duration
        }
      }
    }),
    getReport: async (_, {dateFrom, dateTo, duration = 0}, {models}) => {
      const list = await models.Comment.findAll({
        where: {
          start: {
            [Sequelize.Op.gte]: telephonyLogic.getTimestamp(dateFrom),
            [Sequelize.Op.lte]: telephonyLogic.getTimestamp(dateTo, true)
          },
          duration: {
            [Sequelize.Op.gte]: duration
          }
        }
      });
      return commentsLogic.getReport(dateFrom, dateTo, list);
    }
  },
  Subscription: {
    commentAdded: {
      subscribe:
        () => pubsub.asyncIterator(NEW_COMMENT)
    }
  },
  Mutation: {
    saveComment: async (_, {mark, model, clientName, managerName, objective, moderation, text, id}, {models}) => {
      try {
        /*const call = await models.Call.findOne({where: {entryId}}, {raw: true});
        if (!call) {
          return {
            ok: false
          };
        }
        const comment = await models.Comment.create({entryId: call.entryId, text});

        pubsub.publish(NEW_COMMENT, {commentAdded: {...comment.dataValues, call: call.dataValues}});
        return true;*/
        const comment = {mark, model, clientName, managerName, objective, moderation, text};
        let result = true;
        if (Number.isInteger(id)) {
          await models.Comment.update(comment, {where: {id}});
        } else {
          result = await models.Comment.create(comment);
        }
        return Boolean(result);
      } catch (error) {
        return false;
      }
    }
  }
};
