import {Action} from 'redux-actions';
import {takeLatest, put} from 'redux-saga/effects';

import {Request} from '@utils/request';
import {IClientItem} from '@models/clients';
import * as actions from './actions';

export function* getClients() {
  try {
    const response = yield Request.send({
      query:
        `{
          getClients {
            id
            phone
            name
            price
            active
            deleted
          }
        }`
    });
    const list = response.data.getClients;
    yield put(actions.getClientsSuccess(list));
  } catch (error) {
    yield put(actions.getClientsFailure());
  }
}

export function* saveClient({payload: client}: Action<IClientItem>) {
  try {
    const {id, phone, name = '', price = 0, active = true, deleted = false} = client;
    yield Request.send({
      query:
        `mutation {
          saveClient(
            phone: "${phone}",
            name: "${name}",
            price: "${price}",
            active: "${active}",
            deleted: "${deleted}"
            ${Number.isInteger(id) ? `, id: ${id}` : ''}
          )
        }`
    });
    yield put(actions.saveClientSuccess());
    yield put(actions.getClients());
  } catch (error) {
    yield put(actions.saveClientFailure());
  }
}

export function* deleteClients({payload: ids}: Action<number[]>) {
  try {
    yield Request.send({
      query:
        `mutation {
          deleteClients(ids:[${ids}])
        }`
    });
    yield put(actions.deleteClientsSuccess());
    yield put(actions.getClients());
  } catch (error) {
    yield put(actions.deleteClientsFailure());
  }
}

export default function* sagasManager() {
  yield takeLatest(actions.getClients, getClients);
  yield takeLatest(actions.saveClient, saveClient);
  yield takeLatest(actions.deleteClients, deleteClients);
}
