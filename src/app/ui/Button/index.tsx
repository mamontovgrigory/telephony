import * as React from 'react';
import MaterialButton from '@material-ui/core/Button';

// const style = require('./style.scss');

interface IProps {
  submit?: boolean;
  disabled?: boolean;
  onClick?: (e) => void;
}

interface IState {}

export class Button extends React.Component<IProps, IState> {
  public render() {
    const {submit, disabled, onClick, children} = this.props;
    return (
      <MaterialButton
        variant="contained"
        color="primary"
        type={submit ? 'submit' : 'button'}
        disabled={disabled}
        onClick={onClick}
      >
        {children}
      </MaterialButton>
    );
  }
}
