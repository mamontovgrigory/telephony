import { createAction } from 'redux-actions';

import { ILanguage, IResources } from '@models/localization';

export const setCurrentLanguage = createAction<string>('localization/setCurrentLanguage/SUCCESS');
export const setLanguages = createAction<ILanguage[]>('localization/setLanguages/SUCCESS');
export const setResources = createAction<IResources>('localization/setResources/SUCCESS');
