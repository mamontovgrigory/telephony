import * as React from 'react';
import * as ReactMaterialize from 'react-materialize';
import * as $ from 'jquery';

const style = require('./style.scss');

export interface IProps {
  label?: string | React.ReactNode;
  name?: string;
  password?: boolean;
  validate?: boolean;
  multiple?: boolean;
  onChange?: (value: string) => void;
  sm?: number;
  md?: number;
  lg?: number;
  options: Array<{
    label: string | React.ReactNode;
    value: string | number;
  }>;
  value?: string | string;
}

interface IState {}

export class BaseSelect extends React.Component<IProps, IState> {
  private handleChange = (e) => {
    const {multiple, onChange} = this.props;

    let newValue = e.target.value;
    if (multiple) { // TODO: Get values using native method
      newValue = [];
      const select = $(e.target);
      const ul = select.prev();
      ul.children('li').toArray().forEach((li, i) => {
        if ($(li).hasClass('active')) {
          newValue.push((select.children('option').toArray()[i] as any).value);
        }
      });
    }

    if (onChange) {
      onChange(newValue);
    }
  }

  public render() {
    const { label, name, value, validate, multiple, sm, md, lg, options } = this.props;
    const max = 12;
    let s = sm;
    let m = md;
    let l = lg;
    if (!sm && !md && !lg) {
      s = m = l = max;
    }
    return (
      <ReactMaterialize.Input className={style.Select}
                              label={label}
                              name={name}
                              type="select"
                              validate={validate}
                              multiple={multiple}
                              onChange={this.handleChange}
                              value={value}
                              s={s}
                              m={m}
                              l={l}>
        <option/>
        {options.map(({value, label}, index) => {
          return (<option key={index} value={value}>{label}</option>);
        })}
      </ReactMaterialize.Input>
    );
  }
}
