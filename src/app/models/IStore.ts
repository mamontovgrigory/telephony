import { ILocalizationReducer } from '@models/localization';
import { INavigationReducer } from '@models/navigation';
import { ITelephonyReducer } from '@models/telephony';
import { IUsersReducer } from '@models/users';
import { IClientsReducer } from '@models/clients';

export interface IStore {
  routing: any;
  reduxAsyncConnect: any;
  form: any;
  localization: ILocalizationReducer;
  navigation: INavigationReducer;
  telephony: ITelephonyReducer;
  users: IUsersReducer;
  clients: IClientsReducer;
}
