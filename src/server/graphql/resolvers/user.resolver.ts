const md5 = require('md5');

export default {
  Query: {
    getUser: async (_, {id}, {models}) => models.User.findOne({where: {id}}),
    getUsers: async (_, __, {models}) => models.User.findAll(),
    logIn: async (_, {login, password}, {models}) => models.User.findOne({where: {login, password: md5(password)}})
  },
  Mutation: {
    saveUser: async (_, {login, password: clearPassword, id}, {models}) => {
      try {
        let result = true;
        const password = clearPassword ? md5(clearPassword) : undefined;
        if (Number.isInteger(id)) {
          await models.User.update({login, password}, {where: {id}});
        } else {
          const user = await models.User.create({login, password});
          result = Boolean(user);
        }
        return Boolean(result);
      } catch (err) {
        return false;
      }
    },
    deleteUsers: async (_, {ids}, {models}) => models.User.destroy({where: {id: ids}})
  }
};
