import * as _ from 'lodash';

import {Bind} from './bind';

export class Ioc {
  private static bindings: Bind[] = [];

  public static register<T>(name: string, isSingletone: boolean, obj: any, params?: any): T {
    return this.registerMany<T>([name], isSingletone, obj, params);
  }

  public static registerMany<T>(
    name: string[], isSingletone: boolean, obj: T, params?: any, removeRegistrations: boolean = true): T {
    const bind = new Bind();
    bind.identifiers = name;
    bind.paramsCreate = params;
    bind.isSingletone = isSingletone;
    if (typeof (obj) === 'function') {
      bind.classObj = obj;
    } else {
      bind.isSingletone = true;
      bind.instanceObj = obj;
    }
    if (removeRegistrations) {
      _.remove(this.bindings, (b) => {
        return _.isEqual(b.identifiers.sort(), name.sort());
      });
    }
    this.bindings.push(bind);
    return obj;
  }

  public static initModule<T>(name: string): T {
    return Ioc.resolve<T>(name);
  }

  public static resolve<T>(name: string): T {
    const res: T[] = this.resolvePrivate<T>(name, true);
    if (res.length > 0) {
      return res[0];
    } else {
      return undefined;
    }
  }

  public static resolveMany<T>(name: string): T[] {
    return this.resolvePrivate<T>(name, false);
  }

  private static resolvePrivate<T>(name: string, isSingle: boolean): T[] {
    const res: T[] = [];
    _.forEach(this.bindings, (bind/*, key*/) => {
      const find: string = _.find(bind.identifiers, (ident) => ident === name);
      if (find) {
        if (bind.isSingletone) {
          if (bind.instanceObj) {
            res.push(bind.instanceObj);
            if (isSingle) {
              return false;
            }
          } else {
            bind.instanceObj = new bind.classObj(bind.paramsCreate);
            res.push(bind.instanceObj);
            if (isSingle) {
              return false;
            }
          }
        } else {
          res.push(new bind.classObj(bind.paramsCreate));
          if (isSingle) {
            return false;
          }
        }
      }
    });
    return res;
  }
}
