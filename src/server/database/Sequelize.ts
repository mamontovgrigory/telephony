import Sequelize from 'sequelize';

const config = require('../../../config/main').database;

const sequelize = new Sequelize(config.database, config.user, config.password, {
  host: 'localhost',
  port: 5432,
  dialect: 'postgres',
  define: {
    underscored: true
  },
  operatorsAliases: false
});

export {sequelize, Sequelize};
