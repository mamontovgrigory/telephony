import { createAction } from 'redux-actions';

import {ICallsTotal, ICommentItem, ICallsFilter} from '@models/telephony';

export const getCallsTotal = createAction<{dateFrom: string, dateTo: string}>('comments/getCallsTotal/REQUEST');
export const getCallsTotalSuccess = createAction<ICallsTotal>('comments/getCallsTotal/SUCCESS');
export const getCallsTotalFailure = createAction('comments/getCallsTotal/FAILURE');

export const getComments = createAction<ICallsFilter>('comments/getComments/REQUEST');
export const getCommentsSuccess = createAction<ICommentItem[]>('comments/getComments/SUCCESS');
export const getCommentsFailure = createAction('comments/getComments/FAILURE');

export const saveFilter = createAction<ICallsFilter>('comments/saveFilter');

export const saveComment = createAction<ICommentItem>('comments/saveComment/REQUEST');
export const saveCommentSuccess = createAction('comments/saveComment/SUCCESS');
export const saveCommentFailure = createAction('comments/saveComment/FAILURE');

export const deleteComments = createAction<number[]>('comments/deleteComments/REQUEST');
export const deleteCommentsSuccess = createAction('comments/deleteComments/SUCCESS');
export const deleteCommentsFailure = createAction('comments/deleteComments/FAILURE');

export const getRecord = createAction<string>('comments/getRecord/REQUEST');
export const getRecordSuccess = createAction<string>('comments/getRecord/SUCCESS');
export const getRecordFailure = createAction('comments/getRecord/FAILURE');

export const getReport = createAction<{dateFrom: string, dateTo: string}>('comments/getReport/REQUEST');
export const getReportSuccess = createAction('comments/getReport/SUCCESS');
export const getReportFailure = createAction('comments/getReport/FAILURE');
