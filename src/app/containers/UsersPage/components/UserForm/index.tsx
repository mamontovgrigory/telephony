import * as React from 'react';
const {connect} = require('react-redux');

import { IUserItem } from '@models/users';
import { saveUser } from '@data/users/actions';
import { USER_FORM_NAME } from '@data/users/constants';
import { Loc } from '@containers';
import { Row, Form, Input } from '@ui';

interface IProps {
  saveUser?(item: IUserItem): void;
}

@connect(
  null,
  {
    saveUser
  }
)
export class UserForm extends React.Component<IProps> {
  private handleSubmit = (values) => {
    const { saveUser } = this.props;
    saveUser(values);
  }

  private normalizeLogin = (value: string) => {
    return String(value).replace(/[A-Z]/g, (match) => match.toLowerCase()).replace(/[^a-z0-9@._]/g, '');
  }

  public render() {
    const validate = [(value) => !value];
    return (
      <Form name={USER_FORM_NAME} onSubmit={this.handleSubmit}>
        <Row>
          <Input
            label={<Loc locKey="login"/>}
            name="login"
            validate={validate}
            normalize={this.normalizeLogin}
            required={true}
          />
        </Row>
        <Row>
          <Input
            label={<Loc locKey="password"/>}
            name="password"
            type="password"
          />
        </Row>
      </Form>
    );
  }
}
