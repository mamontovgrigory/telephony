import {ICallDto} from './ICallDto';

export interface ITelephonyLogic {
  getTimestamp(date: string, shift?: boolean): number;

  requestApi(props: {
    command: string;
    data: object;
    csv?: boolean;
    file?: boolean;
  }): Promise<any>;

  getStatistics(dateFrom: string, dateTo: string): Promise<ICallDto[]>;

  downloadRecord(recordId: string): any;
}
