import * as React from 'react';
const {connect} = require('react-redux');
import { createStructuredSelector } from 'reselect';
// const {asyncConnect} = require('redux-connect');

import { IResources } from '@models/localization';
import { IUserItem } from '@models/users';
import { selectCurrentLanguage, selectResources } from '@data/localization/selectors';
import { selectUsersList } from '@data/users/selectors';
import { getUsers, deleteUsers } from '@data/users/actions';
import { USER_FORM_NAME } from '@data/users/constants';
import { Crud } from '@ui';
import { Loc } from '@containers';
import { UserForm } from './components';

interface IProps {
  list: IUserItem[];
  currentLanguage?: string;
  resources?: IResources;
  getUsers(): void;
  deleteUsers(ids: number[]): void;
}

/*@asyncConnect([{
  promise: ({store: {dispatch}}) => dispatch(getUsers())
}])*/
@connect(
  createStructuredSelector({
    currentLanguage: selectCurrentLanguage,
    resources: selectResources,
    list: selectUsersList
  }),
  {
    deleteUsers,
    getUsers
  }
)
export class UsersPage extends React.Component<IProps, {}> {
  public componentDidMount() {
    const {getUsers} = this.props;
    getUsers();
  }

  public render() {
    const {currentLanguage, resources, list, deleteUsers} = this.props;
    const colModel = [
      {
        name: 'id',
        hidden: true
      },
      {
        name: 'groupId',
        hidden: true
      },
      {
        name: 'login',
        label: Loc.getString({currentLanguage, resources, locKey: 'login'})
      },
      {
        name: 'groupName',
        label: Loc.getString({currentLanguage, resources, locKey: 'groupName'})
      }
    ];
    return (
      <Crud
        title={<Loc locKey="users"/>}
        colModel={colModel}
        list={list}
        form={<UserForm/>}
        formName={USER_FORM_NAME}
        deleteItems={deleteUsers}
      />
    );
  }
}
