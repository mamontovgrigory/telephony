import { Action, handleActions } from 'redux-actions';

import { INavigationReducer, INavItem } from '@models/navigation';
import * as actions from './actions';

const initialState: INavigationReducer = {
  navItems: [
    {
      locKey: 'home',
      to: '/',
      icon: 'home'
    },
    {
      locKey: 'users',
      to: '/users',
      icon: 'perm_identity'
    },
    {
      locKey: 'clients',
      to: '/clients',
      icon: 'contact_phone'
    },
    {
      locKey: 'telephony',
      to: '/telephony',
      icon: 'local_phone'
    }
  ]
};

export const navigationReducer = handleActions<INavigationReducer, {}>({
  [(actions.setNavItem as any).toString()]: (state, {payload: navItems}: Action<INavItem[]>) => ({
    ...state,
    navItems
  })
}, initialState);
