import * as React from 'react';
import * as ReactMaterialize from 'react-materialize';

const style = require('./style.scss');

interface IProps {
  centered?: boolean;
  hoverable?: boolean;
  responsive?: boolean;
  striped?: boolean;
  bordered?: boolean;
}

interface IState {}

export class BorderedTable extends React.Component<IProps, IState> {
  public static defaultProps = {
    bordered: true
  };

  public render() {
    return (
      <ReactMaterialize.Table {...this.props} className={style.BorderedTable}/>
    );
  }
}
