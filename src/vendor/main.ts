/**
 * This is an entry point for additional assets,
 * require your assets under this file.
 * Example:
 * require('./bootstrap/css/bootstrap.min.css');
 */
import 'materialize-css';
import 'materialize-css/dist/css/materialize.min.css';
import './material_icons.css';

import './jquery-ui-custom.css'; // TODO: Move to ui Grid
import './grid.scss'; // TODO: Move to ui Grid
