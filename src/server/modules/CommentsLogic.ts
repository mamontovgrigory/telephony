import * as Moment from 'moment';
import {uniq, reduce} from 'lodash';

const fs = require('fs');
const path = require('path');

import {ICommentsLogic, ICallDto, ICommentDto} from '../interfaces';
import {worksheetsLogic} from '../registration';

export class CommentsLogic implements ICommentsLogic {
  private dateFormat: string = 'DD.MM.YYYY';

  public createList(callsList: ICallDto[]) {
    return callsList.map(({entryId, start, finish, fromNumber, toNumber}) => {
      const duration = Moment.unix(Number(finish)).diff(Moment.unix(Number(start)), 'seconds');
      return {
        entryId,
        fromNumber,
        toNumber,
        start: Number(start),
        duration
      };
    });
  }

  private checkIsObjective(value: string) {
    return String(value).toLowerCase().trim() === 'да'; // TODO: Remove cyrillic symbols
  }

  private getClientData(dates: string[], list: ICommentDto[]) {
    const clientData = [];
    dates.forEach((date) => {
      const callsList = list.filter((item) => Moment.unix(item.start).format(this.dateFormat) === date);
      const objectiveList = callsList.filter((item) => this.checkIsObjective(item.objective));
      clientData.push({
        date,
        calls: callsList.length,
        objective: objectiveList.length
      });
    });
    return clientData;
  }

  public getCallsTotal(dateFrom: string, dateTo: string, list: ICommentDto[]) {
    const dates = [];

    const startDay = Moment(dateFrom, this.dateFormat);
    const endDay = Moment(dateTo, this.dateFormat);

    let day = startDay;

    while (day <= endDay) {
      dates.push(day.format(this.dateFormat));
      day = day.clone().add(1, 'd');
    }

    const clientData = this.getClientData(dates, list);
    const calls = reduce(clientData, (sum, item) => sum + item.calls, 0);
    const objective = reduce(clientData, (sum, item) => sum + item.objective, 0);
    const data = {
      dates,
      list: [],
      total: {
        data: clientData,
        total: {
          calls,
          objective
        },
        budget: 0
      }
    };
    const clients = uniq(list.map((item) => item.toNumber));
    clients.forEach((client) => {
      const clientList = list.filter((item) => item.toNumber === client);
      const clientData = this.getClientData(dates, clientList);
      const calls = reduce(clientData, (sum, item) => sum + item.calls, 0);
      const objective = reduce(clientData, (sum, item) => sum + item.objective, 0);
      data.list.push({
        client,
        data: clientData,
        total: {
          calls,
          objective
        },
        budget: 0
      });
    });
    return data;
  }

  public getReport(dateFrom: string, dateTo: string, list: ICommentDto[]) {
    const mapping = {
      start: 'Дата и время',
      fromNumber: 'Исходящий',
      toNumber: 'Входящий',
      duration: 'Длительность',
      mark: 'Марка',
      model: 'Модель',
      clientName: 'Клиент',
      managerName: 'Менеджер',
      text: 'Комментарий',
      objective: 'Целевой'
    };
    const fileName = `${dateFrom}-${dateTo}`;
    const buffer = worksheetsLogic.getMappedBuffer(fileName, mapping, list);

    const dirName = 'public/reports';
    const dir = path.join(__dirname, dirName);
    if (!fs.existsSync(dir)) {
      fs.mkdirSync(dir);
    }
    const filePath = `${dirName}/${fileName}.xlsx`;
    fs.writeFileSync(path.join(__dirname, filePath), buffer);
    return `/${filePath}`;
  }
}
