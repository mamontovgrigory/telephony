/*import * as React from 'react';
import * as  ReactCSSTransitionGroup from 'react-addons-css-transition-group';
const md5 = require('md5');
const style = require('./style.scss');

interface IProps {
  location?: Location;
}

export class Transition extends React.Component<IProps> {
  public render() {
    const {location: {pathname}, children} = this.props;
    const key = md5(pathname);
    const transitionName = {
      enter: style.page_transition_enter,
      enterActive: style.page_transition_enter_active,
      leave: style.page_transition_leave,
      leaveActive: style.page_transition_leave_active,
      appear: style.page_transition_appear,
      appearActive: style.page_transition_appear_active
    };
    return (
      <div className={style.Transition}>
        <ReactCSSTransitionGroup
          key={key}
          component="div"
          transitionName={transitionName}
          transitionAppear={true}
          transitionAppearTimeout={500}
          transitionEnter={true}
          transitionEnterTimeout={500}
          transitionLeave={false}>
          {children}
        </ReactCSSTransitionGroup>
      </div>
    );
  }
}*/
