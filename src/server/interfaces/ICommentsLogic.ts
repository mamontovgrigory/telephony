import {ICallDto} from './ICallDto';
import {ICommentDto} from './ICommentDto';

interface ICallsDataItem {
  date: string;
  calls: number;
  objective: number;
}

interface ICallsData {
  client?: string;
  data: ICallsDataItem[];
  total: {
    calls: number,
    objective: number;
  };
  budget: number;
}

export interface ICommentsLogic {
  createList(callsList: ICallDto[]): ICommentDto[];

  getCallsTotal(dateFrom: string, dateTo: string, list: ICommentDto[]): {
    list: ICallsData[];
    total: ICallsData;
  };

  getReport(dateFrom: string, dateTo: string, list: ICommentDto[]): string;
}
