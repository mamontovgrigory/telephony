// import { isObject, isArray } from 'lodash';

enum IRequestMethod {
  Get = 'GET',
  Delete = 'DELETE',
  Patch = 'PATCH',
  Put = 'PUT',
  Post = 'POST'
}

interface IProps {
  host?: string;
  url?: string;
  data?: any;
  query?: string;
  files?: File[];
}

export class Request {
  public static async send(props) {
    const {host, url, query/*, files*/} = props;
    // const href = (host ? host : 'http://localhost:4000') + '/graphql';
    const href = `${host ? host : `${location.origin}:4000`}${url ? url : '/graphql'}`;
    /*const body = new FormData();
    if (data) {
      for (const key of Object.keys(data)) {
        body.append(key, isObject(data[key]) || isArray(data[key]) ? JSON.stringify(data[key]) : data[key]);
      }
    }
    if (files && files.length) {
      for (const file of files) {
        body.append('files', file);
      }
    }*/
    const response = await fetch(href, {
      method: IRequestMethod.Post,
      // credentials: 'include',
      referrerPolicy: 'origin-when-cross-origin',
      headers: {
        'X-PINGOTHER': 'pingpong',
        'Content-type': 'application/json'
      },
      body: JSON.stringify({
        operationName: null,
        variables: {},
        query
      })
    });
    return response.json();
  }

  public static async post(url: string, props?: IProps) {
    return await Request.send({...props, url});
  };
}
