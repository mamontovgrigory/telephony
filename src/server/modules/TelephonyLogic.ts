import * as Moment from 'moment';
const Papa = require('papaparse');
const fetch = require('node-fetch');
const fs = require('fs');
const path = require('path');
const {URLSearchParams} = require('url');
const sha256 = require('js-sha256').sha256;

import {ITelephonyLogic} from '../interfaces/ITelephonyLogic';

export class TelephonyLogic implements ITelephonyLogic {
  private url: string = 'https://app.mango-office.ru/vpbx';
  private apiKey: string = 'v0rcywmetrowje8kwyde6xa9eswapuha';
  private apiSalt: string = 'kzc9lpd56rrgwqqieumbinan5wnkgvlr';
  private fieldsMapping = {
    records: 'records',
    start: 'start',
    finish: 'finish',
    answer: 'answer',
    fromExtension: 'from_extension',
    fromNumber: 'from_number',
    toExtension: 'to_extension',
    toNumber: 'to_number',
    disconnectReason: 'disconnect_reason',
    lineNumber: 'line_number',
    entryId: 'entry_id',
    location: 'location'
  };
  private dateFormat: string = 'DD.MM.YYYY';

  public getTimestamp(date: string, shift?: boolean) {
    return Moment(date, this.dateFormat).add(shift ? 1 : 0, 'days').unix();
  }

  public async requestApi(props: {
    command: string;
    data: object;
    csv?: boolean;
    file?: boolean;
  }): Promise<any> {
    const {command, data, csv, file} = props;
    const url = `${this.url}${command}`;
    const json = JSON.stringify(data);
    const sign = sha256(this.apiKey + json + this.apiSalt);
    const body = new URLSearchParams();
    body.append('vpbx_api_key', this.apiKey);
    body.append('sign', sign);
    body.append('json', json);
    try {
      const response = await fetch(url, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded; charset=utf-8'
        },
        body
      });
      if (csv) {
        const csvString = await response.text();
        const csvResult = Papa.parse(csvString);
        return csvResult.data;
      }
      if (file) {
        return await response.buffer();
      }
      return await response.json();
    } catch (e) {
      console.log(e);
    }
  }

  public async getStatistics(dateFrom: string, dateTo: string) {
    const fields = Object.values(this.fieldsMapping).join(', ');
    const {key} = await this.requestApi({
      command: '/stats/request',
      data: {
        date_from: String(this.getTimestamp(dateFrom)),
        date_to: String(this.getTimestamp(dateTo)),
        fields,
        from: {
          extension: '',
          number: ''
        },
        to: {
          extension: '',
          number: ''
        }
      }
    });
    const result = await this.requestApi({
      command: '/stats/result',
      csv: true,
      data: {key}
    });
    return result
      .filter((item) => item.length === Object.keys(this.fieldsMapping).length)
      .map((item) => {
        const row = {};
        Object.keys(this.fieldsMapping).forEach((key, index) => {
          row[key] = item[index];
        });
        return row;
      });
  }

  public async downloadRecord(recordId: string) {
    const data = await this.requestApi({
      command: '/queries/recording/post/',
      file: true,
      data: {
        recording_id: recordId,
        action: 'download'
      }
    });

    const dirName = 'public/records';
    const dir = path.join(__dirname, dirName);
    if (!fs.existsSync(dir)) {
      fs.mkdirSync(dir);
    }
    const filePath = `${dirName}/${recordId}.mp3`;
    fs.writeFileSync(path.join(__dirname, filePath), data);
    return `/${filePath}`;
  }
}
