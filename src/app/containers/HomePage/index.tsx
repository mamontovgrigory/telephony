import * as React from 'react';

import { Row, Col } from '@ui';
import { Loc } from '@containers';

export class HomePage extends React.Component {
  public render() {
    return (
      <div>
        <Row>
          <Col>
            <h4>
              <Loc locKey="home"/>
            </h4>
          </Col>
        </Row>
      </div>
    );
  }
}
