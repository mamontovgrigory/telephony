import * as React from 'react';
import ReactAudioPlayer from 'react-audio-player';

interface IProps {
  src: string;
}

export class AudioPlayer extends React.Component<IProps> {
  public render() {
    const {src} = this.props;
    return (
      <ReactAudioPlayer
        src={location.origin + src}
        autoPlay={false}
        controls={true}
      />
    );
  };
}
