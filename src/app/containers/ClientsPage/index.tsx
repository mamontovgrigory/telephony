import * as React from 'react';
const {connect} = require('react-redux');
import { createStructuredSelector } from 'reselect';
const {asyncConnect} = require('redux-connect');

import { IResources } from '@models/localization';
import { IClientItem } from '@models/clients';
import { selectCurrentLanguage, selectResources } from '@data/localization/selectors';
import { selectClientsList } from '@data/clients/selectors';
import { getClients, deleteClients } from '@data/clients/actions';
import { CLIENT_FORM_NAME } from '@data/clients/constants';
import { Crud } from '@ui';
import { Loc } from '@containers';

interface IProps {
  list: IClientItem[];
  currentLanguage?: string;
  resources?: IResources;
  deleteClients(ids: number[]): void;
}

@asyncConnect([{
  promise: ({store: {dispatch}}) => dispatch(getClients())
}])
@connect(
  createStructuredSelector({
    currentLanguage: selectCurrentLanguage,
    resources: selectResources,
    list: selectClientsList
  }),
  {
    deleteClients
  }
)
export class ClientsPage extends React.Component<IProps, {}> {
  public render() {
    const {currentLanguage, resources, list, deleteClients} = this.props;
    const colModel = [
      {
        name: 'id',
        hidden: true
      },
      {
        name: 'phone',
        label: Loc.getString({currentLanguage, resources, locKey: 'phone'})
      },
      {
        name: 'name',
        label: Loc.getString({currentLanguage, resources, locKey: 'alias'})
      },
      {
        name: 'price',
        label: Loc.getString({currentLanguage, resources, locKey: 'price'})
      }
    ];
    return (
      <Crud
        title={<Loc locKey="clients"/>}
        colModel={colModel}
        list={list}
        form={<div/>}
        formName={CLIENT_FORM_NAME}
        deleteItems={deleteClients}
      />
    );
  }
}
