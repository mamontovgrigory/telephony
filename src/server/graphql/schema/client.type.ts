export default `
  type Client {
    id: Int!
    phone: String!
    name: String
    price: Int
    active: Boolean
    deleted: Boolean
  }
  
  type Mutation {
    saveClient(phone: String!, name: String!, price: Int!, active: Boolean!, deleted: Boolean!, id:Int): Boolean!
    deleteClients(ids:[Int]!): Boolean!
  }

  type Query {
    getClient(id:Int!): Client
    getClients: [Client]!
  }
`;
