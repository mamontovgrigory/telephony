import * as React from 'react';
// import * as Helmet from 'react-helmet';
import { createStructuredSelector } from 'reselect';
const {connect} = require('react-redux');

const {version} = require('../../../../package.json');
// const appConfig = require('../../../../config/main');
import { IAccount } from '@models/users';
import { ILanguage, IResources } from '@models/localization';
import { INavItem } from '@models/navigation';
import {
  setCurrentLanguage,
  setLanguages,
  setResources
} from '@data/localization/actions';
import { setNavItem } from '@data/navigation/actions';
import { selectAccount } from '@data/users/selectors';
import { selectNavItems } from '@data/navigation/selectors';
import { logout } from '@data/users/actions';
// import { Transition } from '@ui';
import { Toolbar } from '@ui';
import { LoginPage/*, Log*/ } from '@containers';
const style = require('./style.scss');
const languages: ILanguage[] = require('./languages.json');
const resources = require('./resources.json');

interface IProps {
  location?: Location;
  account?: IAccount;
  navItems?: INavItem[];
  setCurrentLanguage?(languageCode: string): void;
  setLanguages?(languages: ILanguage[]): void;
  setResources?(resources: IResources): void;
  setNavItem?(navItems: INavItem[]): void;
  logout?(): void;
}

@connect(
  createStructuredSelector({
    account: selectAccount,
    navItems: selectNavItems
  }),
  {
    setCurrentLanguage,
    setLanguages,
    setResources,
    setNavItem,
    logout
  }
)
export class App extends React.Component<IProps> {
  public componentDidMount() {
    const {setCurrentLanguage, setLanguages, setResources} = this.props;
    const currentLanguage: string = languages[0].code;
    setCurrentLanguage(currentLanguage);
    setLanguages(languages);
    setResources(resources);
  }

  public render() {
    const {/*location, */account, navItems, children, logout} = this.props;
    /*const accountItem = account ? {
      name: account.login
    } : null;*/
    return (
      <section className={style.App}>
        {/*<Helmet {...appConfig.app} {...appConfig.app.head}/>*/}
        <Toolbar
          version={version}
          login={account && account.login}
          menuItems={navItems}
          logout={logout}
        />
        <div className={style.page}>
          {account ? children : <LoginPage/>}
          {/*<Transition location={location}>
            {account ? children : <LoginPage/>}
          </Transition>*/}
        </div>
        {/*<Log/>*/}
      </section>
    );
  }
}
