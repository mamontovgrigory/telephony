export interface IUserItem {
  id?: number;
  login?: string;
  password?: string;
  groupId?: number;
}

export interface IAccountPermissions {
  usersManage: boolean;
  groupsManage: boolean;
}

export interface IAccount extends IUserItem {
  permissions: IAccountPermissions;
}

export interface IUsersReducer {
  isFetching?: boolean;
  account?: IAccount;
  list: IUserItem[];
  error?: boolean;
  message?: any;
}
