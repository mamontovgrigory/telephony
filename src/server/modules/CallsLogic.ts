import * as Moment from 'moment';
import {uniq, reduce} from 'lodash';

import {ICallDto, ICallsLogic} from '../interfaces';

export class CallsLogic implements ICallsLogic {
  public dateFormat: string = 'DD.MM.YYYY';

  public getClientData(dates: string[], list: ICallDto[]) {
    const clientData = [];
    dates.forEach((date) => {
      const calls = list.filter((item) => Moment.unix(Number(item.start)).format(this.dateFormat) === date).length;
      clientData.push({
        date,
        calls,
        objective: 0
      });
    });
    return clientData;
  }

  public getCallsTotal(dateFrom: string, dateTo: string, list: ICallDto[]) {
    const dates = [];

    const startDay = Moment(dateFrom, this.dateFormat);
    const endDay = Moment(dateTo, this.dateFormat);

    let day = startDay;

    while (day <= endDay) {
      dates.push(day.format(this.dateFormat));
      day = day.clone().add(1, 'd');
    }

    const clientData = this.getClientData(dates, list);
    const calls = reduce(clientData, (sum, item) => sum + item.calls, 0);
    const data = {
      dates,
      list: [],
      total: {
        data: clientData,
        total: {
          calls,
          objective: 0
        },
        budget: 0
      }
    };
    const clients = uniq(list.map((item) => item.toNumber));
    clients.forEach((client) => {
      const clientList = list.filter((item) => item.toNumber === client);
      const clientData = this.getClientData(dates, clientList);
      const calls = reduce(clientData, (sum, item) => sum + item.calls, 0);
      data.list.push({
        client,
        data: clientData,
        total: {
          calls,
          objective: 0
        },
        budget: 0
      });
    });
    return data;
  }
}
