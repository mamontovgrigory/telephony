import { createSelector } from 'reselect';

import { IStore } from '@models/IStore';
import { IClientsReducer, IClientItem } from '@models/clients';

const selectDomain = (state: IStore): IClientsReducer => state.clients;

export const selectClientsList = createSelector<IStore, IClientsReducer, IClientItem[]>(
  selectDomain,
  (substate) => substate && substate.list
);
