export interface IWorksheetsLogic {
  parse(file: string): any;

  getMappedBuffer(name: string, mapping: any, list: any): any;

  getBuffer(name: string, data: any): any;

  getMappedData(mapping: any, file: any): any;
}
