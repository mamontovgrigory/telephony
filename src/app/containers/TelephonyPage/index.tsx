import * as React from 'react';
import * as Moment from 'moment';
import { first, last } from 'lodash';
// import {browserHistory} from 'react-router-dom';
import {createStructuredSelector} from 'reselect';

const {connect} = require('react-redux');

import {ICallsTotal, ICallsFilter} from '@models/telephony';
import {searchFormValuesSelector, selectCallsTotal} from '@data/telephony/selectors';
import {getCallsTotal, getComments, getReport} from '@data/telephony/actions';
import {SEARCH_FORM_NAME} from '@data/telephony/constants';
import {BorderedTable, Button, Col, Row, Form, DatePicker, Input} from '@ui';
import {Loc} from '@containers';

const style = require('./style.scss');

interface IProps {
  dateFrom: string;
  dateTo: string;
  callsTotal?: ICallsTotal;

  getReport(data: {dateFrom: string, dateTo: string}): void;
  getComments?(data: ICallsFilter): void;
  getCallsTotal?(data: { dateFrom: string, dateTo: string }): void;
}

@connect(
  createStructuredSelector({
    callsTotal: selectCallsTotal,
    dateFrom: (state) => searchFormValuesSelector(state, 'dateFrom'),
    dateTo: (state) => searchFormValuesSelector(state, 'dateTo')
  }),
  {
    getCallsTotal,
    getComments,
    getReport
  }
)
export class TelephonyPage extends React.Component<IProps> {
  private initialValues = {
    dateFrom: Moment().subtract(6, 'days').format('DD.MM.YYYY'),
    dateTo: Moment().format('DD.MM.YYYY'),
    duration: 0
  };

  private getAllClients = () => {
    const {callsTotal} = this.props;
    if (!callsTotal) {
      return [];
    }
    return callsTotal.list.map((item) => item.client);
  }

  private getAllDates = () => {
    const {callsTotal} = this.props;
    if (!callsTotal) {
      return [];
    }
    return callsTotal.dates;
  }

  private renderDataCell = (data, client?: string) => {
    const content = (
      <span>
        {data.calls} <span className={style.footnote}>({data.objective})</span>
      </span>
    );
    if (client) {
      const onClick = () => this.openCallsPage(client, data.date);
      return (
        <td className={style.cell} onClick={onClick}>
          {content}
        </td>
      );
    }
    const onClick = () => this.openCallsPage(null, data.date);
    return (
      <th className={style.cell} onClick={onClick}>
        {content}
      </th>
    );
  }

  private renderClientRow = (item, index) => (
    <tr key={index}>
      <td>{item.client}</td>
      {item.data.map((data) => this.renderDataCell(data, item.client))}
      {this.renderDataCell(item.total, item.client)}
      <td>{item.budget}</td>
    </tr>
  )

  public renderTable = () => {
    const {callsTotal} = this.props;
    if (!callsTotal) {
      return null;
    }
    return (
      <Row>
        <Col>
          <BorderedTable>
            <thead>
            <tr>
              <th>
                <Loc locKey="clients"/>
              </th>
              {callsTotal.dates.map((header) => <th>{header}</th>)}
              <th>
                <Loc locKey="total"/>
              </th>
              <th>
                <Loc locKey="budget"/>
              </th>
            </tr>
            </thead>
            <tbody>
            {callsTotal.list.map((item, index) => this.renderClientRow(item, index))}
            <tr>
              <th>
                <Loc locKey="total"/>
              </th>
              {callsTotal.total.data.map((data) => this.renderDataCell(data))}
              {this.renderDataCell(callsTotal.total.total)}
              <th>{callsTotal.total.budget}</th>
            </tr>
            </tbody>
          </BorderedTable>
        </Col>
      </Row>
    );
  }

  private openCallsPage = (client?: string, date?: string) => {
    const {getComments} = this.props;
    const clients = client ? [client] : this.getAllClients();
    const dates = this.getAllDates();
    const dateFrom = date ? date : first(dates);
    const dateTo = date ? date : last(dates);
    getComments({
      clients,
      dateFrom,
      dateTo
    });
    // browserHistory.push('/calls');
  }

  private handleClickReport = () => {
    const {dateFrom, dateTo, getReport} = this.props;
    getReport({dateFrom, dateTo});
  }

  private handleSubmit = (values) => {
    const {getCallsTotal} = this.props;
    getCallsTotal(values);
  }

  public render() {
    return (
      <div className={style.Telephony}>
        <Row>
          <Col>
            <h4>
              <Loc locKey="telephony"/>
            </h4>
          </Col>
        </Row>
        <Form name={SEARCH_FORM_NAME} initialValues={this.initialValues} onSubmit={this.handleSubmit}>
          <Row>
            <Col>
              <DatePicker name="dateFrom" sm={4} label={<Loc locKey="periodFrom"/>}/>
              <DatePicker name="dateTo" sm={4} label={<Loc locKey="periodTo"/>}/>
              <Input name="duration" type="number" sm={4} label={<Loc locKey="durationInSeconds"/>}/>
            </Col>
          </Row>
          <Row>
            <Col>
              <Button submit={true}>
                <Loc locKey="search"/>
              </Button>
              <Button onClick={this.handleClickReport}>
                <Loc locKey="report"/>
              </Button>
            </Col>
          </Row>
        </Form>
        {this.renderTable()}
      </div>
    );
  }
}
