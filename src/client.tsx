import 'babel-polyfill';
import * as e6p from 'es6-promise';
(e6p as any).polyfill();
import 'isomorphic-fetch';

import * as React from 'react';
import * as ReactDOM from 'react-dom';
import * as moment from 'moment-timezone';
import { Provider } from 'react-redux';
const { Router } = require('react-router-dom');
import { syncHistoryWithStore } from 'react-router-redux';
const { createBrowserHistory } = require('history');
const { ReduxAsyncConnect } = require('redux-connect');
import { configureStore } from './app/redux/store';
import 'isomorphic-fetch';
import routes from './app/routes';

const browserHistory = createBrowserHistory();

moment.tz.setDefault('Europe/Moscow');
const store = configureStore(
  browserHistory,
  window.__INITIAL_STATE__
);
const history = syncHistoryWithStore(browserHistory, store);
const connectedCmp = (props) => <ReduxAsyncConnect {...props} />;

ReactDOM.render(
  <Provider store={store} key="provider">
    <Router
      history={history}
      render={connectedCmp}
    >
      {routes}
    </Router>
  </Provider>,
  document.getElementById('app')
);
