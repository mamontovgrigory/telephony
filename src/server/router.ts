import * as express from 'express';
const formidable = require('formidable');

import { ExpressSessionTuner } from './middlewares/common/ExpressSessionTuner';
import { ExpressCommonTuner } from './middlewares/common/ExpressCommonTuner';
import './graphql';

const app = express();
ExpressCommonTuner.Setup(app);
ExpressSessionTuner.Setup(app);
export const apiRouter = express.Router();
app.use('/Api', apiRouter);
app.use('/Api', ( req, res, next ) => {
  try {
    const form = new formidable.IncomingForm();
    form.parse(req, async (err, fields) => {
      if (err) {
        res.json({
          url: req.url,
          error: err,
          fields
        });
      } else {
        req.body = fields;
        next();
      }
    });
  } catch (error) {
    res.json({
      url: req.url,
      error: error.errorType,
      message: error.message
    });
  }
});

export const router = app;
