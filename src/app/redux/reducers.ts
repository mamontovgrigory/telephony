import { combineReducers, Reducer } from 'redux';
import { routerReducer } from 'react-router-redux';
import { reducer as formReducer } from 'redux-form';
const {reducer} = require('redux-connect');

import { localizationReducer } from '@data/localization/reducer';
import { clientsReducer } from '@data/clients/reducer';
import { navigationReducer } from '@data/navigation/reducer';
import { telephonyReducer } from '@data/telephony/reducer';
import { usersReducer } from '@data/users/reducer';
import { IStore } from '@models/IStore';

const rootReducer: Reducer<IStore> = combineReducers<IStore>({
  routing: routerReducer,
  reduxAsyncConnect: reducer,
  form: formReducer,
  localization: localizationReducer,
  navigation: navigationReducer,
  telephony: telephonyReducer,
  clients: clientsReducer,
  users: usersReducer
});

export default rootReducer;
