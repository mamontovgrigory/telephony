import { createAction } from 'redux-actions';

import { INavItem } from '@models/navigation';

export const setNavItem = createAction<INavItem[]>('navigation/setNavItem/SUCCESS');
