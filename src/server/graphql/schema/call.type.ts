export default `
  type Call {
    id: Int!
    records: String!
    start: String!
    finish: String!
    answer: String!
    fromExtension: String!
    fromNumber: String!
    toExtension: String!
    toNumber: String!
    disconnectReason: String!
    entryId: String!
    lineNumber: String!
    location: String!
    comment: Comment
  }
  
  type CallsDataItem {
    date: String
    calls: Int!
    objective: Int!
  }
  
  type CallsData {
    client: String
    data: [CallsDataItem]!
    total: CallsDataItem!
    budget: Int!   
  }
  
  type CallsTotal {
    dates: [String]! 
    list: [CallsData]!
    total: CallsData!      
  }
  
  type Query {
    getCallsTotal(dateFrom: String!, dateTo: String!): CallsTotal!
    getRecord(recordId: String!): String!
  }
  
  type Mutation {
    # Update telephony statistics
    updateStatistics(dateFrom: String, dateTo: String): Boolean!
  }
`;
