import * as React from 'react';
import * as ReactMaterialize from 'react-materialize';

interface IProps {
  progress?: number;
}

interface IState {}

export class ProgressBar extends React.Component<IProps, IState> {
  public render() {
    const { progress } = this.props;
    return (
      <ReactMaterialize.ProgressBar progress={progress ? progress : undefined} />
    );
  }
}
