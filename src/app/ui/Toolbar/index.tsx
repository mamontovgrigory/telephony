import * as React from 'react';
import { withStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import MaterialToolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';

import { Menu } from '@ui';

interface IProps {
  version: string;
  login?: string;
  menuItems: any[];
  logout?(): void;
}

const styles = {
  root: {
    flexGrow: 1
  },
  grow: {
    flexGrow: 1
  },
  menuButton: {
    marginLeft: -12,
    marginRight: 20
  }
};

const TopAppBar: React.FC<IProps> = (props: IProps & {classes: any}) => {
  const { classes, version, login, menuItems, logout } = props;
  return (
    <div className={classes.root}>
      <AppBar position="static">
        <MaterialToolbar>
          {login && <Menu menuItems={menuItems} />}
          <Typography variant="h6" color="inherit" className={classes.grow}>
            {version}
          </Typography>
          {login && <Button color="inherit" onClick={logout}>Logout</Button>}
        </MaterialToolbar>
      </AppBar>
    </div>
  );
};

export const Toolbar = withStyles(styles)(TopAppBar);
